'use strict'

const { expect } = require('chai')
const assert = require('assert')
const MaskanCoreChaincode = require('./chaincode/maskan-core')
const Blockchain = require('./blockchain')
const Utils = require('./utils')
const fabricNetwork = require('fabric-network')
const randToken = require('rand-token').generator({ chars: 'numeric' })
const path = require('path')
const _ = require('lodash')

const walletPath = path.join(__dirname, 'wallet')

let adminGateway = new fabricNetwork.Gateway()
const wallet = new fabricNetwork.FileSystemWallet(walletPath)

const dayInMS = (24 * 60 * 60 * 1000)

let serverAdminUser

const users = [
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
  randToken.generate(10),
]

const seller = {
  users: [users[0], users[1], users[2], users[3]],
  signingRules: [
    {
      and: {
        signers: [users[0], users[1]]
      }
    },
    {
      outof: {
        mustSign: 2,
        signers: [users[0], users[2], users[3]]
      }
    }
  ],
  walletId: randToken.generate(12),
  executerId: '6549683225'
}

const buyer = {
  users: [users[4], users[5], users[6], users[7]],
  signingRules: [
    {
      and: {
        signers: [users[4], users[5]]
      }
    },
    {
      outof: {
        mustSign: 2,
        signers: [users[5], users[6], users[7]]
      }
    }
  ],
  walletId: randToken.generate(12),
  executerId: '9071696570'
}

const purchaseId = randToken.generate(16)

const payables = [
  {
    id: `${purchaseId}_0`,
    price: 1000,
    dueDate: Date.now(),
  },
  {
    id: `${purchaseId}_1`,
    price: 2000,
    dueDate: Date.now(),
  }
]

const receivableTerms = [
  {
    tokenType: 'Barat',
    dueDate: Date.now()
  }
]

const invoiceIds = [
  randToken.generate(25)
]

let approvePurchaseRequest;

const tokenSettings = {
  price: 1000,
  maxCustomTransaction: 1000,
  maxTokenTransaction: 10000,
  duePeriod: 365
}

const tokenId = randToken.generate(12)
const baratToken = {
  name: `Test_${randToken.generate(10)}`,
  price: 1000,
  issuedAt: (Date.now() + 1 * dayInMS).toString(),
  duePeriod: 365
}

const fundIds = [randToken.generate(16), randToken.generate(16)]
let approveFundRequest;

const payableSettlementIds = [
  randToken.generate(8),
  randToken.generate(8)
]

const payableTransfers = [
  [{
    transferId: randToken.generate(6),
    tokenId,
    amount: 100
  }],
  [{
    transferId: randToken.generate(6),
    tokenId,
    amount: 500
  }]
]

let approvePayableSettlementBySender
let approvePayableSettlementByReceiver

before(async () => {
  await Blockchain.ca.enrollAdmin()
  serverAdminUser = await Utils.User.createRandomUser(5, 'ADMIN')
  
  await Utils.User.createUsers(users, 'REAL')

  adminGateway = await Utils.Gateway.createGateway(serverAdminUser)

  const MaskanWallet = new Utils.Wallet(adminGateway)
  await MaskanWallet.updateCaCertificate()

  await MaskanWallet.createRealWallets(users)
  await MaskanWallet.createCorpWallets([seller, buyer])

  if (!await MaskanCoreChaincode.getTokenSettings(adminGateway)) {
    await MaskanCoreChaincode.createTokenSettings(
      tokenSettings.price,
      tokenSettings.duePeriod,
      tokenSettings.maxCustomTransaction,
      tokenSettings.maxTokenTransaction,
      adminGateway)
  }

  await MaskanCoreChaincode.createBaratToken(
    tokenId,
    baratToken.name,
    baratToken.issuedAt,
    adminGateway)
})

after(async () => {
  adminGateway.disconnect()
})

describe('Purchase', () => {
  it('Should create purchase', async () => {
    const sellerId = seller.walletId
    const buyerId = buyer.walletId

    const { purchase, request } = await MaskanCoreChaincode.createPurchase(purchaseId, sellerId, buyerId, adminGateway)
    approvePurchaseRequest = request
    const expectedRequest = {
      walletId: sellerId,
      personId: seller.executerId,
      requestType: 'ADD_SELL_ORDER',
      status: 'CREATED'
    }
    expect(request).to.include(expectedRequest)

    const expectedPurchase = { sellerId: seller.walletId, buyerId: buyer.walletId }
    expect(purchase).to.include(expectedPurchase)
  })

  it('Should update sell order', async () => {
    const expiresAt = (new Date(2022)).toString()
    const { purchase, request } = await MaskanCoreChaincode.addPurchaseSellOrder(purchaseId, payables, receivableTerms, invoiceIds, expiresAt, adminGateway)

    approvePurchaseRequest = request;

    // for (const payable of purchase.payables) {
      // expect(purchase.payables).to.deep.equal({ ...payables, status: 'PENDING' })
    // }
    expect(purchase.receivableTerms).to.deep.equal(receivableTerms)
  })

  it('Should approve sell order by seller', async () => {
    let requestId = approvePurchaseRequest.id
    let requestData = approvePurchaseRequest.requestData

    const sellerUsers = seller.users
    for (const user of sellerUsers) {
      console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedOrder = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const { purchase, request } = await MaskanCoreChaincode.approvePurchase(requestId, requestData, signedOrder, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        console.log(request);
        if (request) {
          console.log(request);
          approvePurchaseRequest = request
        }
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })

  it('Should approve sell order by buyer', async () => {
    const requestId = approvePurchaseRequest.id
    const requestData = approvePurchaseRequest.requestData
    const buyerUsers = buyer.users
    for (const user of buyerUsers) {
      // console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedOrder = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const { purchase, request } = await MaskanCoreChaincode.approvePurchase(requestId, requestData, signedOrder, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })

  // it('Should create purchase by third party', async () => {
  //   const sellerId = seller.walletId
  //   const buyerId = buyer.walletId
  //   const expiresAt = (new Date(2022)).toString()
  //   const thirdPartyId = randToken.generate(6)

  //   const purchase = await MaskanCoreChaincode.createPurchaseByThirdParty(purchaseId,
  //                                                                         sellerId,
  //                                                                         buyerId,
  //                                                                         expiresAt,
  //                                                                         payables,
  //                                                                         receivableTerms,
  //                                                                         invoiceIds,
  //                                                                         thirdPartyId,
  //                                                                         adminGateway)
  //   // console.log(purchase);
  // })

  // it('Should approve purchase created by third party', async () => {
  //   const purchase = await MaskanCoreChaincode.approveThirdPartyPurchaseByAdmin(purchaseId, adminGateway)
  //   // console.log(purchase);
  // })

  it('Should create fund request', async () => {
    const result = await MaskanCoreChaincode.createFundRequest(fundIds[0], payables[0].id, adminGateway)
    // console.log(result)
    approveFundRequest = result.request
  })
  it('Should approve fund request by buyer members', async () => {
    const { id: requestId, requestData } = approveFundRequest
    for (const user of buyer.users) {
      const pki = Utils.User.readUserPki(user)
      const signedFund = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approveFundRequestByBuyer(requestId, requestData, signedFund, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result)
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })
  it('Should approve fund request by admin', async () => {
    const result = await MaskanCoreChaincode.approveFundByAdmin(fundIds[0], 'MASKAN_ADMIN', 'ENDORSE', adminGateway)
    // console.log(result)
  })
  it('Should raise fund', async () => {
    const transactionId = randToken.generate(16)
    const result = await MaskanCoreChaincode.raiseFund(fundIds[0], transactionId, tokenId, 1000, adminGateway)
    // console.log(result)
  })

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  it('Should create fund request', async () => {
    const result = await MaskanCoreChaincode.createFundRequest(fundIds[1], payables[1].id, adminGateway)
    // console.log(result)
    approveFundRequest = result.request
  })
  it('Should approve fund request by buyer members', async () => {
    const { id: requestId, requestData } = approveFundRequest
    for (const user of buyer.users) {
      const pki = Utils.User.readUserPki(user)
      const signedFund = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approveFundRequestByBuyer(requestId, requestData, signedFund, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result)
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })
  it('Should approve fund request by admin', async () => {
    const result = await MaskanCoreChaincode.approveFundByAdmin(fundIds[1], 'MASKAN_ADMIN', 'ENDORSE', adminGateway)
    // console.log(result)
  })
  it('Should raise fund', async () => {
    const transactionId = randToken.generate(16)
    const result = await MaskanCoreChaincode.raiseFund(fundIds[1], transactionId, tokenId, 2000, adminGateway)
    // console.log(result)
  })
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  

  it('Should create a payable transfer', async () => {
    const result = await MaskanCoreChaincode.createPayableSettlement(payableSettlementIds[0], payables[0].id, payableTransfers[0], adminGateway)
    // console.log(result);
    approvePayableSettlementBySender = result.request
  })

  it('Should approve payable transfer by sender', async () => {
    const { id: requestId, requestData } = approvePayableSettlementBySender
    const senderUsers = buyer.users
    for (const user of senderUsers) {
      // console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedPayableTransfer = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approvePayableSettlement(requestId, requestData, signedPayableTransfer, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result);
        if (result.requests && result.requests[1]) {
          approvePayableSettlementByReceiver = result.requests[1]
        }
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })

  it('Should approve payable transfer by receiver', async () => {
    const { id: requestId, requestData } = approvePayableSettlementByReceiver
    const receiverUsers = seller.users
    for (const user of receiverUsers) {
      // console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedPayableTransfer = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approvePayableSettlement(requestId, requestData, signedPayableTransfer, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result);
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  it('Should create a payable transfer', async () => {
    const result = await MaskanCoreChaincode.createPayableSettlement(payableSettlementIds[1], payables[1].id, payableTransfers[1], adminGateway)
    // console.log(result);
    approvePayableSettlementBySender = result.request
  })
  it('Should approve payable transfer by sender', async () => {
    const { id: requestId, requestData } = approvePayableSettlementBySender
    const senderUsers = buyer.users
    for (const user of senderUsers) {
      // console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedPayableTransfer = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approvePayableSettlement(requestId, requestData, signedPayableTransfer, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result);
        if (result.requests && result.requests[1]) {
          approvePayableSettlementByReceiver = result.requests[1]
        }
      } catch(ex) {
        let reverted = false
        // console.log(`Request with Id: ${requestId} not found`);
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })
  it('Should approve payable transfer by receiver', async () => {
    const { id: requestId, requestData } = approvePayableSettlementByReceiver
    const receiverUsers = seller.users
    for (const user of receiverUsers) {
      // console.log(user);
      const pki = Utils.User.readUserPki(user)
      const signedPayableTransfer = Utils.Cryptography.signPKCS1(requestData, pki.key)
      try {
        const result = await MaskanCoreChaincode.approvePayableSettlement(requestId, requestData, signedPayableTransfer, pki.cert, 'RSA-SHA1', 'ENDORSE', adminGateway)
        // console.log(result);
      } catch(ex) {
        let reverted = false
        reverted = ex.endorsements[0].message.includes(`Request with Id: ${requestId} not found`)
        assert.strictEqual(reverted, true)
      }
    }
  })
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
})
