const forge = require('node-forge')
const rsasign = require('jsrsasign')
const asn1 = rsasign.asn1
const crypto = require('crypto')
const x509 = require('@ampretia/x509')

class CryptoUtil {
  static getKeypairRSA (keysize) {
    const keypair = forge.pki.rsa.generateKeyPair(keysize)

    const prvKeyObj = {
      prvKeyPem: forge.pki.privateKeyToPem(keypair.privateKey)
    }

    const pubKeyObj = {
      pubKeyPem: forge.pki.publicKeyToPem(keypair.publicKey)
    }

    return {
      prvKeyObj,
      pubKeyObj
    }
  }

  static async geneateCSRwithRSA (subject, keypair) {
    const subjectDN = this._generateLdapSubject(subject)

    const csri = new asn1.csr.CertificationRequestInfo()
    csri.setSubjectByParam({ str: subjectDN })
    csri.setSubjectPublicKeyByGetKey(keypair.pubKeyObj.pubKeyPem)

    const csr = new asn1.csr.CertificationRequest({ csrinfo: csri })

    csr.asn1SignatureAlg = new asn1.x509.AlgorithmIdentifier({
      name: 'SHA256withRSA'
    })

    const signature = this.signPKCS1(csr.asn1CSRInfo.getEncodedHex(), keypair.prvKeyObj.prvKeyPem, 'hex', 'RSA-SHA256', 'hex')
    csr.hexSig = signature
    csr.asn1Sig = new asn1.DERBitString({ hex: '00' + csr.hexSig })
    const seq = new asn1.DERSequence({
      array: [csr.asn1CSRInfo, csr.asn1SignatureAlg, csr.asn1Sig]
    })
    csr.hTLV = seq.getEncodedHex()
    csr.isModified = false

    const csrPEM = csr.getPEMString()

    return csrPEM
  }

  static signPKCS1 (
    tbs,
    key,
    tbsEncoding = 'utf8',
    algorithm = 'RSA-SHA1',
    signEncoding = 'base64'
  ) {
    const buffer = Buffer.from(tbs, tbsEncoding)
    const signer = crypto.createSign(algorithm)
    signer.update(buffer)
    const signature = signer.sign(key).toString(signEncoding)

    return signature
  }

  static normalizeCert (certificate) {
    if (!certificate.startsWith('-----BEGIN CERTIFICATE-----')) {
      certificate =
        '-----BEGIN CERTIFICATE-----\n' +
        certificate +
        '\n-----END CERTIFICATE-----'
    }
    return certificate
  }

  static verifySignPKCS1 (
    tbs,
    certificate,
    signature,
    tbsEncoding = 'utf8',
    algorithm = 'RSA-SHA1',
    signEncoding = 'base64'
  ) {
    algorithm = this.convertSignAlgorithm(algorithm)
    const buffer = Buffer.from(tbs, tbsEncoding)
    const verifier = crypto.createVerify(algorithm)
    verifier.update(buffer)
    const verified = verifier.verify(certificate, signature, signEncoding)

    return verified
  }

  static convertSignAlgorithm (algorithm) {
    if (algorithm === 'SHA1withRSA') {
      return 'RSA-SHA1'
    }
    if (algorithm === 'SHA224withRSA') {
      return 'RSA-SHA224'
    }
    if (algorithm === 'SHA256withRSA') {
      return 'RSA-SHA256'
    }
    if (algorithm === 'SHA384withRSA') {
      return 'RSA-SHA384'
    }
    if (algorithm === 'SHA512withRSA') {
      return 'RSA-SHA512'
    }
    if (algorithm === 'MD5withRSA') {
      return 'RSA-MD5'
    }
    return algorithm
  }

  static _generateLdapSubject (subject) {
    const subjectKeys = Object.keys(subject)
    let subjectDN = ''

    for (const k of subjectKeys) {
      subjectDN = `${subjectDN}/${k}=${subject[k]}`
    }

    return subjectDN
  }

  static getCertificateSubject (certificate) {
    const normalizedCert = this.normalizeCert(certificate)
    const issuerSubject = x509.getSubject(normalizedCert)
    return issuerSubject
  }

  static getIssuerSubject (certificate) {
    const normalizedCert = this.normalizeCert(certificate)
    const issuerSubject = x509.getIssuer(normalizedCert)
    return issuerSubject
  }
}

module.exports = CryptoUtil
