const Blockchain = require('../blockchain')
const fabricNetwork = require('fabric-network')
const path = require('path')

module.exports = class Gateway {
  static async createGateway(id) {
    const walletPath = path.join(__dirname, '..', 'wallet')
    const wallet = new fabricNetwork.FileSystemWallet(walletPath)

    const connectionProfile = await Blockchain.connection.getConnection()
  
    const options = {
      wallet,
      identity: id,
      discovery: {
        asLocalhost: true,
        enabled: true
      }
    }
  
    const gateway = new fabricNetwork.Gateway()
    await gateway.connect(connectionProfile, options)

    return gateway
  }
}