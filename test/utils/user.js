const CA = require('../blockchain/ca-util')
const randToken = require('rand-token').generator({ chars: 'numeric' })
const path = require('path')
const fs = require('fs')

module.exports = class User {
  static async createRandomUser(idLength, role) {
    const userId = `${role}_${randToken.generate(idLength)}`

    const registerRequest = {
      affiliation: 'org1.department1',
      enrollmentID: userId,
      role: 'client',
      maxEnrollments: 3,
      attrs: [{ name: 'maskanRole', value: role, ecert: true }]
    }

    await CA.registerUserWithECDSA(registerRequest)

    return userId
  }

  static async createUser(userId, role) {
    const registerRequest = {
      affiliation: 'org1.department1',
      enrollmentID: userId,
      role: 'client',
      maxEnrollments: 3,
      attrs: [{ name: 'maskanRole', value: role, ecert: true }]
    }

    await CA.registerUserWithECDSA(registerRequest)
  }

  static async createUsers(userIds, role) {
    await Promise.all(
      userIds.map(async user => {
        await this.createUser(user, role)
      })
    )
  }

  static readUserPki(userId) {
    const enrollmentPath = path.join(__dirname, '..', 'wallet', `${userId}`, `${userId}`)
    const enrollment = JSON.parse(fs.readFileSync(enrollmentPath).toString())
    const ski = enrollment.enrollment.signingIdentity
    
    const cert = enrollment.enrollment.identity.certificate

    const keyPath = path.join(__dirname, '..', 'wallet', `${userId}`, `${ski}-priv`)
    const key = fs.readFileSync(keyPath).toString()

    return {
      cert,
      key
    }
  }
}
