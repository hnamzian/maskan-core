const MaskanCore = require('../chaincode/maskan-core')
const SigningRules = require('./signingRules')
const path =require('path')
const fs = require('fs');

module.exports = class Wallet {
  constructor(gateway) {
    this.gateway = gateway
  }

  async updateCaCertificate() {
    const certPath = path.join(__dirname, '..', 'certs', 'CACertificate.cert')
    const caCert = fs.readFileSync(certPath).toString()
    return await MaskanCore.updateCACertificate(caCert, this.gateway);
  }

  async getWallet(walletId) {
    const wallet = await MaskanCore.getWallet(walletId, this.gateway)
    return wallet
  }

  async createRealWallet(walletId) {
    let { wallet, request } = await MaskanCore.createRealWallet(walletId, this.gateway)
    wallet = await MaskanCore.approveWalletByAdmin(request.id, walletId, 'ENDORSE', this.gateway)
    return wallet;
  }

  async createRealWallets(walletIds) {
    for (const walletId of walletIds) {
      console.log(walletId);
      await this.createRealWallet(walletId)
    }
  }

  async createCorpWallet(walletId, signingRules, executerId) {
    let { wallet, request } = await MaskanCore.createCorpWallet(walletId, signingRules, executerId, this.gateway)
    const signers = SigningRules.getSigners(wallet.signingRules)

    for (const user of signers) {
      await MaskanCore.approveWalletByAdmin(request.id, user, 'ENDORSE' ,this.gateway)
    }

    return await this.getWallet(walletId)
  }

  async createCorpWallets(wallets) {
    for (const wallet of wallets) {
      await this.createCorpWallet(wallet.walletId, wallet.signingRules, wallet.executerId)
    }
  }
}