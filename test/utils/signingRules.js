module.exports = class SigningRules {
  static getSigners(signingRules) {
    let signers = [];
    for (const rule of signingRules) {
      signers.push(...rule[Object.keys(rule)[0]].signers);
    }
    signers = signers.filter((elem, pos) => {
      return signers.indexOf(elem) === pos;
    });

    return signers;
  }

  static verifySigningRule(signers, signingRules) {
    for (const signingRule of signingRules) {
      const op = Object.keys(signingRule)[0];
      if (op === 'and') {
        if (this.verifySigningRulesAND(signers, signingRule[op].signers)) {
          return true;
        }
      } else if (op === 'outof') {
        if (this.verifySigningRulesOUTOF(signers, signingRule[op].signers, signingRule[op].mustSign)) {
          return true;
        }
      }
    }
    return false;
  }

  verifySigningRulesAND(signers, signingRule) {
    for (const ruleSigner of signingRule) {
      if (signers.indexOf(ruleSigner) < 0) {
        return false;
      }
    }
    return true;
  }

  verifySigningRulesOUTOF(signers, signingRule, outof) {
    let numberOdSigners = 0;

    for (const ruleSigner of signingRule) {
      if (signers.indexOf(ruleSigner) >= 0) { numberOdSigners++; }
    }

    return numberOdSigners >= outof;
  }
}
  