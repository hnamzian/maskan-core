'use strict'

const assert = require('assert')
const MaskanCoreChaincode = require('./chaincode/maskan-core')
const { Cryptography, User } = require('./utils')
const Blockchain = require('./blockchain')
const Utils = require('./utils')
const fabricNetwork = require('fabric-network')
const randToken = require('rand-token').generator({ chars: 'numeric' })
const fs = require('fs')
const path = require('path')
const _ = require('lodash')


let adminGateway = new fabricNetwork.Gateway()

let serverAdminUser
let user1 = `${randToken.generate(10)}_mobile`
let user2 = `${randToken.generate(10)}_mobile`
let user3 = `${randToken.generate(10)}_mobile`

const executer = `${randToken.generate(10)}_mobile`
const corpWallet1 = randToken.generate(16)
const corpWallet2 = randToken.generate(16)
const corpWallet3 = randToken.generate(16)
const corpWallet4 = randToken.generate(16)
const corpWallet5 = randToken.generate(16)
const signingRules = [
  {
    and: {
      signers: [user1.split('_')[0], user2.split('_')[0]]
    },
    outof: {
      mustSign: 1,
      signers: [user1.split('_')[0], user2.split('_')[0]]
    }
  }
]

before(async () => {
  await Blockchain.ca.enrollAdmin()
  serverAdminUser = await Utils.User.createRandomUser(5, 'ADMIN')
  await Utils.User.createUser(user1, 'REAL')
  await Utils.User.createUser(user2, 'REAL')
  await Utils.User.createUser(user3, 'REAL')

  adminGateway = await Utils.Gateway.createGateway(serverAdminUser)
})

beforeEach(async () => {
})

after(async () => {
  adminGateway.disconnect()
})

describe('CA Certificate', () => {
  it('Should update CA Certificate', async () => {
    const certPath = path.join(__dirname, 'certs', 'CACertificate.cert')
    const certificate = fs.readFileSync(certPath).toString()
    const caCert = await MaskanCoreChaincode.updateCACertificate(certificate, adminGateway)
    const { commonName: caId } = Cryptography.getCertificateSubject(certificate)
    assert.strictEqual(caCert.id, caId)
    assert.strictEqual(certificate, caCert.certificate)
  })

})

describe('Wallet', () => {
  it('Should succesfully create REAL type wallet and activated by admin', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(user1, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const requestId = request.id
    const { wallet: approvedWallet, request: approvedRequest } = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user1, 'ENDORSE' ,adminGateway)
    assert.strictEqual(approvedWallet.status, 'ACTIVATED')
    assert.strictEqual(approvedRequest.status, 'APPROVED')
  })

  it('Should succesfully create REAL type wallet and activated by user', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(user2, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')
    
    const { id: requestId, requestData } = request

    const { key, cert } = User.readUserPki(user2)
    const signedRequestData = Utils.Cryptography.signPKCS1(requestData, key)
    const { wallet: approvedWallet, request: approvedRequest } = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestData, cert,  'SHA1withRSA', 'ENDORSE', adminGateway)
    assert.strictEqual(approvedWallet.status, 'ACTIVATED')
    assert.strictEqual(approvedRequest.status, 'APPROVED')
  })

  it('Should succesfully create REAL type wallet and rejected by admin', async () => {
    const userId = randToken.generate(10)
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(userId, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request
    
    const { wallet: rejectedWallet, request: rejectedRequest } = await MaskanCoreChaincode.approveWalletByAdmin(requestId, userId, 'REJECT', adminGateway)
    assert.strictEqual(rejectedWallet.status, 'SUSPENDED')
    assert.strictEqual(rejectedRequest.status, 'REJECTED')
  })

  it('Should succesfully create REAL type wallet for user2 and deny endorsement by user1', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(user3, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')
    
    const { id: requestId, requestData } = request

    const { key, cert } = User.readUserPki(user1)
    const signedRequestData = Utils.Cryptography.signPKCS1(requestData, key)
    let reverted = false
    try {
      await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestData, cert,  'SHA1withRSA', 'ENDORSE', adminGateway)
    } catch (ex) {
      reverted = ex.message.includes(`does not have permission to sign this request`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create REAL type wallet and deny endorsement by admin on-behalf of another user', async () => {
    const userId = randToken.generate(10)
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(userId, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request
    const corruptedUser = randToken.generate(10)
    let reverted = false
    try {
      await MaskanCoreChaincode.approveWalletByAdmin(requestId, corruptedUser, 'ENDORSE', adminGateway)
    } catch (ex) {
      reverted = ex.message.includes(`does not have permission to sign this request`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create REAL type wallet and deny rejection by admin on-behalf of another user', async () => {
    const userId = randToken.generate(10)
    const { wallet, request } = await MaskanCoreChaincode.createRealWallet(userId, adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request
    const corruptedUser = randToken.generate(10)
    let reverted = false
    try {
      await MaskanCoreChaincode.approveWalletByAdmin(requestId, corruptedUser, 'REJECT', adminGateway)
    } catch (ex) {
      reverted = ex.message.includes(`does not have permission to sign this request`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create CORP type wallet and activated by admin', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWallet1, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request

    let result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user1, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'PENDING')
    assert.strictEqual(result.request.status, 'PENDING')
    
    result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user2, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'ACTIVATED')
    assert.strictEqual(result.request.status, 'APPROVED')
  })

  it('Should succesfully create CORP type wallet and approved by users', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWallet2, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId, requestData } = request
    
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    let result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'PENDING')
    assert.strictEqual(result.request.status, 'PENDING')

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)
    result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'ACTIVATED')
    assert.strictEqual(result.request.status, 'APPROVED')
  })

  it('Should succesfully create CORP type wallet and approve by user1 itself and user2 on-behalf', async () => {
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWallet3, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId, requestData } = request

    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    let result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'PENDING')
    assert.strictEqual(result.request.status, 'PENDING')
    
    result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user2, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'ACTIVATED')
    assert.strictEqual(result.request.status, 'APPROVED')
  })

  it('Should succesfully create CORP type wallet and rejected by admin on-behalf of user1', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request
    
    let result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user1, 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')
    
    let reverted;
    try {
      result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user2, 'REJECT' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create CORP type wallet and rejected by admin on-behalf of user2', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId } = request

    let result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user1, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'PENDING')
    assert.strictEqual(result.request.status, 'PENDING')
    
    result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user2, 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')
  })

  it('Should succesfully create CORP type wallet and rejected by admin for all users', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')
    assert.strictEqual(request.status, 'PENDING')

    const { id: requestId } = request
    
    let result = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user1, 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')
    
    let reverted;
    try {
      wallet = await MaskanCoreChaincode.approveWalletByAdmin(requestId, user2, 'ENDORSE' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create CORP type wallet and rejected by user1', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')

    const { id: requestId, requestData } = request
    
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    let result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)

    let reverted = false;
    try {
      result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create CORP type wallet and approve by all users', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')
    assert.strictEqual(request.status, 'PENDING')

    const { id: requestId, requestData } = request
    
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    let result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)

    let reverted = false;
    try {
      result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should succesfully create CORP type wallet and approve by user2', async () => {
    const corpWalletId = randToken.generate(16)
    const { wallet, request } = await MaskanCoreChaincode.createCorpWallet(corpWalletId, signingRules, '', adminGateway)
    assert.strictEqual(wallet.status, 'PENDING')
    assert.strictEqual(request.status, 'PENDING')

    const { id: requestId, requestData } = request

    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    let result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'PENDING')
    assert.strictEqual(result.request.status, 'PENDING')

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)

    result = await MaskanCoreChaincode.approveWalletByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.wallet.status, 'SUSPENDED')
    assert.strictEqual(result.request.status, 'REJECTED')
  })

  it('Should Succesfuly create request to update executor of corp wallet and approved by admin (user1) and user2', async () => {
    const executerId = `${randToken.generate(10)}`

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    result = await MaskanCoreChaincode.approveWalletUpdateByAdmin(requestId, user1, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')
    

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)
    result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.request.status, 'APPROVED')
    assert.strictEqual(result.wallet.executerId, executerId)    
  })

  it('Should create request to update wallet executor and rejected by admin on-behalf of user2', async () => {
    const executerId = `${randToken.generate(10)}`

    const corpWallet = await MaskanCoreChaincode.getWallet(corpWallet1, adminGateway)

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    result = await MaskanCoreChaincode.approveWalletUpdateByAdmin(requestId, user1, 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')
    
    result = await MaskanCoreChaincode.approveWalletUpdateByAdmin(requestId, user2, 'REJECT' ,adminGateway)
    assert.strictEqual(result.request.status, 'REJECTED')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)
  })

  it('Should create request to update wallet executor and rejected by admin on-behalf of user1', async () => {
    const executerId = `${randToken.generate(10)}`

    const corpWallet = await MaskanCoreChaincode.getWallet(corpWallet1, adminGateway)

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    result = await MaskanCoreChaincode.approveWalletUpdateByAdmin(requestId, user1, 'REJECT' ,adminGateway)
    assert.strictEqual(result.request.status, 'REJECTED')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)

    let reverted = false;
    try {
      result = await MaskanCoreChaincode.approveWalletUpdateByAdmin(requestId, user2, 'ENDORSE' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should create request to update executor of corp wallet and rejected by user1', async () => {
    const executerId = `${randToken.generate(10)}`

    const corpWallet = await MaskanCoreChaincode.getWallet(corpWallet1, adminGateway)

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.request.status, 'REJECTED')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)


    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)

    let reverted = false;
    try {
      result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should create request to update executor of corp wallet and rejected by all users', async () => {
    const executerId = `${randToken.generate(10)}`

    const corpWallet = await MaskanCoreChaincode.getWallet(corpWallet1, adminGateway)

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.request.status, 'REJECTED')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)
    

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)
    let reverted = false;
    try {
      result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    } catch(ex) {
      reverted = ex.message.includes(`Request with Id: ${requestId} not found`)
    }
    assert.strictEqual(reverted, true)
  })

  it('Should create request to update executor of corp wallet and rejected by user2', async () => {
    const executerId = `${randToken.generate(10)}`

    const corpWallet = await MaskanCoreChaincode.getWallet(corpWallet1, adminGateway)

    const walletUpdate = { executerId }
    let result = await MaskanCoreChaincode.createWalletUpdate(corpWallet1, walletUpdate, adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')

    const { id: requestId, requestData } = result.request
    const user1Pki = User.readUserPki(user1)
    const signedRequestDataByUser1 = Utils.Cryptography.signPKCS1(requestData, user1Pki.key)
    result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser1, user1Pki.cert,  'SHA1withRSA', 'ENDORSE' ,adminGateway)
    assert.strictEqual(result.request.status, 'PENDING')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)
    

    const user2Pki = User.readUserPki(user2)
    const signedRequestDataByUser2 = Utils.Cryptography.signPKCS1(requestData, user2Pki.key)
    result = await MaskanCoreChaincode.approveWalletUpdateByMembers(requestId, requestData, signedRequestDataByUser2, user2Pki.cert,  'SHA1withRSA', 'REJECT' ,adminGateway)
    assert.strictEqual(result.request.status, 'REJECTED')
    assert.strictEqual(result.wallet.executerId, corpWallet.executerId)
  })
})

