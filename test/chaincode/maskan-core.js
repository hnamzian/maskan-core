const Blockchain = require('../blockchain')

module.exports = class MaskanCore {
  static async updateCACertificate (certificate, gateway) {
    const chaincode = 'maskan-core'
    const func = 'updateCACertificate'
    const args = [certificate]
    const caCert = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return caCert ? JSON.parse(caCert) : null
  }

  static async getCACertificate (caId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'getCACertificate'
    const args = [caId]
    const caCert = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return caCert ? JSON.parse(caCert) : null
  }

  static async getWallet (walletId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'getWallet'
    const args = [walletId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async getCorpWallets (walletId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'getCorpWallets'
    const args = [walletId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async getCorpWalletsByQuery (walletId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'getCorpWalletsByQuery'
    const args = [walletId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createRealWallet (walletId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createRealWallet'
    const args = [walletId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createCorpWallet (walletId, signingRules, executerId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createCorpWallet'
    const args = [walletId, JSON.stringify(signingRules), executerId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveWalletByMembers(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveWalletByMembers'
    const args = [requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveWalletByAdmin(requestId, signer, signMethod, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveWalletByAdmin'
    const args = [requestId, signer, signMethod]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createWalletUpdate(walletId, walletUpdateString, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createWalletUpdate'
    const args = [walletId, JSON.stringify(walletUpdateString)]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveWalletUpdateByAdmin(requestId, signer, signMethod, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveWalletUpdateByAdmin'
    const args = [requestId, signer, signMethod]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveWalletUpdateByMembers(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveWalletUpdateByMembers'
    const args = [requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createPurchase(purchaseId, typedSellerId, typedBuyerId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createPurchase'
    const args = [purchaseId, typedSellerId, typedBuyerId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async addPurchaseSellOrder(purchaseId, payables, receivableTerms, invoiceIds, expiresAt, gateway) {
    const chaincode = 'maskan-core'
    const func = 'addPurchaseSellOrder'
    const args = [purchaseId,  JSON.stringify(payables), JSON.stringify(receivableTerms), JSON.stringify(invoiceIds), expiresAt]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approvePurchase (requestId, rawOrder, signedOrder, certificate, signAlgorithm, signMethod, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approvePurchase'
    const args = [requestId, rawOrder, signedOrder, certificate, signAlgorithm, signMethod]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createPurchaseByThirdParty(purchaseId, sellerId, buyerId, expiresAt, payables, receivableTerms, invoiceIds, thirdPartyId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createPurchaseByThirdParty'
    const args = [purchaseId, sellerId, buyerId, expiresAt, JSON.stringify(payables), JSON.stringify(receivableTerms), JSON.stringify(invoiceIds), thirdPartyId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveThirdPartyPurchaseByAdmin(purchaseId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveThirdPartyPurchaseByAdmin'
    const args = [purchaseId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createFundRequest(fundId, payableId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createFundRequest'
    const args = [fundId, payableId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveFundRequestByBuyer(requestId, rawRequest, signedRequest, certificate, signAlgorithm, approvement, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveFundRequestByBuyer'
    const args = [requestId, rawRequest, signedRequest, certificate, signAlgorithm, approvement]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approveFundByAdmin(fundId, adminId, approvement, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approveFundByAdmin'
    const args = [fundId, adminId, approvement]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async raiseFund(fundId, transactionId, tokenId, amount, gateway) {
    const chaincode = 'maskan-core'
    const func = 'raiseFund'
    const args = [fundId, transactionId, tokenId, amount.toString()]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async getTokenSettings(gateway) {
    const chaincode = 'maskan-core'
    const func = 'getTokenSettings'
    const args = []
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return result.length > 0 ? JSON.parse(result.toString()) : null
  }

  static async createTokenSettings(price, duePeriod, maxCustomTransaction, maxTokenTransaction, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createTokenSettings'
    const args = [price.toString(), duePeriod.toString(), maxCustomTransaction.toString(), maxTokenTransaction.toString()]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async updateTokenSettings(baratTokenSettingsUpdate, gateway) {
    const chaincode = 'maskan-core'
    const func = 'updateTokenSettings'
    const args = [JSON.stringify(baratTokenSettingsUpdate)]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createPayableSettlement(payableSettlementId, payableId, transfers, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createPayableSettlement'
    const args = [payableSettlementId, payableId, JSON.stringify(transfers)]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async approvePayableSettlement(requestId, rawRequest, signedRequest, certificate, signAlgorithm, approvement, gateway) {
    const chaincode = 'maskan-core'
    const func = 'approvePayableSettlement'
    const args = [requestId, rawRequest, signedRequest, certificate, signAlgorithm, approvement]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async getBaratToken(tokenId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'getBaratToken'
    const args = [tokenId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async createBaratToken(tokenId, tokenName, issuedAt, gateway) {
    const chaincode = 'maskan-core'
    const func = 'createBaratToken'
    const args = [tokenId, tokenName, issuedAt]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async updateBaratToken(tokenId, tokenName, issueAt, gateway) {
    const chaincode = 'maskan-core'
    const func = 'updateBaratToken'
    const args = [tokenId, tokenName, issueAt]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }

  static async removeToken(tokenId, gateway) {
    const chaincode = 'maskan-core'
    const func = 'removeToken'
    const args = [tokenId]
    const result = await Blockchain.chaincode.submitTransaction(chaincode, func, args, gateway)
    return JSON.parse(result.toString())
  }
}
