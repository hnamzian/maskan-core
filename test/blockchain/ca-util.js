const ConnectionUtil = require('./connection-util')
const CryptoUtil = require('../utils/cryptography')
const FabricCAServices = require('fabric-ca-client')
const { FileSystemWallet, Gateway, X509WalletMixin } = require('fabric-network')
const path = require('path')

const walletPath = path.join(__dirname, '../', 'wallet')
const adminCAId = 'adminCA'

class CaUtil {
  static async enrollAdmin () {
    try {
      if (await this.existsIdentity(adminCAId)) return

      const caInfo = ConnectionUtil.getCaInfo()

      const ca = new FabricCAServices(caInfo)

      const enrollment = await ca.enroll({
        enrollmentID: 'admin',
        enrollmentSecret: 'adminpw'
      })

      await this.importIdentity(
        ConnectionUtil.getMspId(),
        adminCAId,
        enrollment.certificate,
        enrollment.key.toBytes(),
        walletPath
      )
    } catch (error) {
      throw new Error(`Failed to enroll admin user "admin": ${error}`)
    }
  }

  static async enrollUser (enrollmentID, enrollmentSecret, connectionOptions, csr = null) {
    const ca = new FabricCAServices(connectionOptions)

    let enrollmentRequest = {
      enrollmentID,
      enrollmentSecret
    }
    enrollmentRequest = csr ? { ...enrollmentRequest, csr } : enrollmentRequest
    const enrollment = await ca.enroll(enrollmentRequest)

    return enrollment
  }

  static async registerUser (registerRequest) {
    const connectionProfile = ConnectionUtil.getConnection()

    const wallet = new FileSystemWallet(walletPath)

    const enrollmentId = registerRequest.enrollmentID

    const userExists = await wallet.exists(enrollmentId)
    if (userExists) return

    const adminExists = await wallet.exists(adminCAId)
    if (!adminExists) { throw Error(`An identity for ${adminCAId} does not exist in the wallet`) }

    const gateway = new Gateway()
    await gateway.connect(connectionProfile, {
      wallet,
      identity: adminCAId,
      discovery: { enabled: false }
    })

    const ca = gateway.getClient().getCertificateAuthority()
    const adminCAIdentity = gateway.getCurrentIdentity()

    const secret = await ca.register(registerRequest, adminCAIdentity)

    return secret
  }

  static async registerUserWithRSA (regRequest) {
    const keypair = await CryptoUtil.getKeypairRSA(2048)
    const csr = await CryptoUtil.geneateCSRwithRSA({ CN: regRequest.enrollmentID }, keypair)
    const secret = await this.registerUser(regRequest)
    const enrollment = await this.enrollUser(regRequest.enrollmentID, secret, ConnectionUtil.getCaInfo(), csr)
    return {
      privKey: keypair.prvKeyObj.prvKeyPem,
      certificate: enrollment.certificate,
      rootCertificate: enrollment.rootCertificate,
      enrollmentId: regRequest.enrollmentID
    }
  }

  static async registerUserWithECDSA (regRequest) {
    if (await this.existsIdentity(regRequest.enrollmentID)) return
    const secret = await this.registerUser(regRequest)
    const enrollment = await this.enrollUser(regRequest.enrollmentID, secret, ConnectionUtil.getCaInfo())
    await this.importIdentity(
      ConnectionUtil.getMspId(),
      regRequest.enrollmentID,
      enrollment.certificate,
      enrollment.key.toBytes(),
      walletPath
    )
  }

  static async importIdentity (mspId, enrollmentId, certificate, key, walletPath) {
    const wallet = new FileSystemWallet(walletPath)

    const userExists = await wallet.exists(enrollmentId)
    if (userExists) throw Error(`An identity for ${enrollmentId} already exists`)

    const userIdentity = X509WalletMixin.createIdentity(mspId, certificate, key)

    await wallet.import(enrollmentId, userIdentity)
  }

  static async existsIdentity (enrollmentId) {
    const wallet = new FileSystemWallet(walletPath)
    const userExists = await wallet.exists(enrollmentId)
    return userExists
  }
}

module.exports = CaUtil
