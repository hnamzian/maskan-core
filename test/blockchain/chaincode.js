/*
 * This file contains functions for the use of your test file.
 * It doesn't require any changes for immediate use.
 */

'use strict'
const URL = require('url')

class SmartContractUtil {
  static async submitTransaction (contractName, functionName, args, gateway) {
    // Submit transaction
    const network = await gateway.getNetwork('maskan')
    const contract = await network.getContract(contractName)
    const responseBuffer = await contract.submitTransaction(functionName, ...args)
    return responseBuffer
  }

  static async evaluateTransaction (contractName, functionName, args, gateway) {
    // Submit transaction
    const network = await gateway.getNetwork('maskan')
    const contract = await network.getContract(contractName, contractName)
    const responseBuffer = await contract.evaluateTransaction(functionName, ...args)
    return responseBuffer
  }

  // Checks if URL is localhost
  static isLocalhostURL (url) {
    const parsedURL = URL.parse(url)
    const localhosts = ['localhost', '127.0.0.1']
    return localhosts.indexOf(parsedURL.hostname) !== -1
  }

  // Used for determining whether to use discovery
  static hasLocalhostURLs (connectionProfile) {
    const urls = []
    for (const nodeType of ['orderers', 'peers', 'certificateAuthorities']) {
      if (!connectionProfile[nodeType]) {
        continue
      }
      const nodes = connectionProfile[nodeType]
      for (const nodeName in nodes) {
        if (!nodes[nodeName].url) {
          continue
        }
        urls.push(nodes[nodeName].url)
      }
    }
    return urls.some(url => this.isLocalhostURL(url))
  }
}

module.exports = SmartContractUtil
