const fs = require('fs')
const path = require('path')

class ConnectionUtil {
  static getConnection () {
    const connectionProfilePath = path.join(__dirname, '..', 'connection', 'connection.json')
    const connectionProfileContents = fs.readFileSync(connectionProfilePath)
    return JSON.parse(connectionProfileContents)
  }

  static getCaInfo () {
    const ccp = this.getConnection()

    const orgName = ccp.client.organization
    const caInfo =
      ccp.certificateAuthorities[
        ccp.organizations[orgName].certificateAuthorities
      ]
    return caInfo
  }

  static getMspId () {
    const ccp = this.getConnection()

    const orgName = ccp.client.organization

    return ccp.organizations[orgName].mspid
  }
}

module.exports = ConnectionUtil
