MOUNT_LOCAL_Path="/home/hossein/workspace/fabric-samples/chaincode"
MOUNT_FABRIC_PATH="/opt/gopath/src/github.com"

CHAINCODE_RELATIVE_PATH="/maskan/maskan-core"

CHAINCODE_LOCAL_PATH="${MOUNT_LOCAL_Path}${CHAINCODE_RELATIVE_PATH}"
CHAINCODE_FABRIC_PATH="${MOUNT_FABRIC_PATH}${CHAINCODE_RELATIVE_PATH}"

CHAINCODE_NAME="maskan-core"
CHANNEL_NAME="maskan"

rm -rf dist
npm run build
cp -r src dist package.json tsconfig.json $CHAINCODE_LOCAL_PATH

while getopts "p:v:" opt
do
   case "$opt" in
      v ) VERSION="$OPTARG" ;;
      p ) PROCESS="$OPTARG" ;;
   esac
done

docker exec -i cli sh -c "peer chaincode install -n ${CHAINCODE_NAME} -p $CHAINCODE_FABRIC_PATH -v ${VERSION} -l node"

docker exec -i cli sh -c "peer chaincode ${PROCESS} -n ${CHAINCODE_NAME} -v ${VERSION} -c '{\"Args\":[]}' -C ${CHANNEL_NAME}"
