import { Context, Info, Transaction } from 'fabric-contract-api';
import { BaratTokenSettingsEntity } from './barat-token/barat-token-settings.entity';
import { BaratTokenProvider } from './barat-token/barat-token.provider';
import { BaseChaincode } from './base-chaincode';
import { CaCertEntity } from './ca-cert/ca-cert.entity';
import { CaCertificateProvider } from './ca-cert/ca-cert.provider';
import { Configs } from './configs';
import { ContextProvider } from './core/context/context.provider';
import { ExecRequestEntity } from './exec-request/exec-request.entity';
import { FundEntity } from './fund/fund.entity';
import { FundProvider } from './fund/fund.provider';
import { MultiSigRequestEntity, SignMethod } from './multisig-request/multisig-request.entity';
import { PayableSettlementTransfer } from './payable/payable-settlement.entity';
import { PayableEntity } from './payable/payable.entity';
import { PayableProvider } from './payable/payable.provider';
import { IReceivableTerm, PurchaseEntity } from './purchase/purchase.entity';
import { PurchaseProvider } from './purchase/purchase.provider';
import { TagEntity } from './tags/tag.entity';
import { TagProvider } from './tags/tag.provider';
import { WalletEntity } from './wallet/wallet.entity';
import { WalletProvider } from './wallet/wallet.provider';
import { WalletUtils } from './wallet/wallet.utils';
import { TagTypeEntity } from './tags/tag-type.entity';

@Info({ title: 'MaskanCoreChaincode', description: 'Maskan Core Smart Contract' })
export class MaskanCoreChaincode extends BaseChaincode {
  @Transaction(true)
  public async updateCACertificate(ctx: Context, certificate: string): Promise<CaCertEntity> {
    // create Ca Certificate
    const caCertificateProvider = new CaCertificateProvider(ctx);
    const caCert = await caCertificateProvider.updateCACertificate(certificate);

    // raise CA_CERTIFICATE_UPDATED event
    ContextProvider.raiseEvent(ctx, Configs.events.caCertificate, caCert);

    // reurn data
    return caCert;
  }

  @Transaction(false)
  public async getCACertificate(ctx: Context, caId: string): Promise<string> {
    const caCertificateProvider = new CaCertificateProvider(ctx);
    const caCert = await caCertificateProvider.getCACertificate(caId);
    return caCert.certificate;
  }

  @Transaction(false)
  public async getWallet(ctx: Context, walletId: string): Promise<WalletEntity> {
    const walletProvider = new WalletProvider(ctx);
    const wallet = await walletProvider.getWallet(walletId);
    return wallet;
  }

  @Transaction(false)
  public async getConectedWallets(ctx: Context, walletId: string): Promise<{ wallet: WalletEntity, corpWallets: WalletEntity[] }> {
    const walletProvider = new WalletProvider(ctx);
    const wallets = await walletProvider.getConnectedWallets(walletId);
    return wallets;
  }

  @Transaction()
  public async createRealWallet(ctx: Context, walletId: string): Promise<{ wallet: WalletEntity, request: MultiSigRequestEntity }> {
    // create real type wallet
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.createRealWallet(walletId);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // return wallet
    return { wallet, request };
  }

  @Transaction()
  public async createCorpWallet(ctx: Context, walletId: string, signingRulesString: string, executerId: string): Promise<{ wallet: WalletEntity, request: MultiSigRequestEntity }> {
    // parse signingRules
    const signingRules = JSON.parse(signingRulesString);

    // create wallet object
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.createCorpWallet(walletId, signingRules, executerId);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // return wallet
    return { wallet, request };
  }

  @Transaction()
  public async approveWalletByAdmin(ctx: Context, requestId: string, signer: string, signMethod: SignMethod): Promise<{ wallet: WalletEntity, request: MultiSigRequestEntity }> {
    // Approve Request
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.approveWalletByAdmin(requestId, signer, signMethod);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // Return data
    return { wallet, request };
  }

  @Transaction()
  public async approveWalletByMembers(ctx: Context,
                                      requestId: string,
                                      rawRequest: string,
                                      signedRequest: string,
                                      certificate: string,
                                      signingAlg: string,
                                      signMethod: SignMethod): Promise<{ wallet: WalletEntity, request: MultiSigRequestEntity }> {
    // approve wallet by user directly
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.approveWlletByMember(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // Return data
    return { wallet, request };
  }

  @Transaction()
  public async createWalletUpdate(ctx: Context, walletId: string, walletUpdateString: string): Promise<{ wallet: WalletEntity, request: MultiSigRequestEntity }> {
    const walletProvider = new WalletProvider(ctx);
    const walletUpdate = JSON.parse(walletUpdateString);
    const { wallet, tasks, request } = await walletProvider.createWalletUpdate(walletId, walletUpdate);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    return { wallet, request };
  }

  @Transaction()
  public async approveWalletUpdateByAdmin(ctx: Context, requestId: string, signer: string, signMethod: SignMethod) {
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.approveWalletUpdateByAdmin(requestId, signer, signMethod);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // Return data
    return { wallet, request };
  }

  @Transaction()
  public async approveWalletUpdateByMembers(ctx: Context,
                                            requestId: string,
                                            rawRequest: string,
                                            signedRequest: string,
                                            certificate: string,
                                            signingAlg: string,
                                            signMethod: SignMethod) {
    const walletProvider = new WalletProvider(ctx);
    const { wallet, tasks, request } = await walletProvider.approveWalletUpdateByMember(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // create tasks and raise event
    const eventData = { tasks, wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    // Return data
    return { wallet, request };
  }

  @Transaction()
  public async addTagsToWallet(ctx: Context, walletId: string, tagIds: string[]) {
    const walletProvider = new WalletProvider(ctx);
    const wallet = await walletProvider.addTags(walletId, tagIds);

    // raise event
    const eventData = { wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    return wallet;
  }

  @Transaction()
  public async removeTagsFromWallet(ctx: Context, walletId: string, tagIds: string[]) {
    const walletProvider = new WalletProvider(ctx);
    const wallet = await walletProvider.removeTags(walletId, tagIds);

    // raise event
    const eventData = { wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    return wallet;
  }

  @Transaction()
  public async updateWalletTags(ctx: Context, walletId: string, tagIds: string[]) {
    const walletProvider = new WalletProvider(ctx);
    const wallet = await walletProvider.updateTags(walletId, tagIds);

    // raise event
    const eventData = { wallet };
    ContextProvider.raiseEvent(ctx, Configs.events.walletUpdate, eventData);

    return wallet;
  }

  @Transaction()
  public async createPurchase(ctx: Context, purchaseId: string, typedSellerId: string, typedBuyerId: string, orderString: string): Promise<{ purchase: PurchaseEntity, request: ExecRequestEntity }> {
    const purchaseProvider = new PurchaseProvider(ctx);
    const { walletId: sellerId } = WalletUtils.parseWalletId(typedSellerId);
    const { walletId: buyerId } = WalletUtils.parseWalletId(typedBuyerId);
    const purchaseOrder = JSON.parse(orderString);
    const { purchase, request, task } = await purchaseProvider.createPurchase(purchaseId, sellerId, buyerId, purchaseOrder);

    // raise event
    const eventData = { purchase, tasks: [ task ] };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    // return data
    return { purchase, request };
  }

  @Transaction()
  public async addPurchaseSellOrder(ctx: Context,
                                    purchaseId: string,
                                    payablesString: string,
                                    receivablesString: string,
                                    invoiceIdsString: string,
                                    expiresAt: string) {
    // parse inputs
    const payablesArray: PayableEntity[] = JSON.parse(payablesString);
    const receivables: IReceivableTerm[] = JSON.parse(receivablesString);
    const invoiceIds = JSON.parse(invoiceIdsString);

    const purchaseProvider = new PurchaseProvider(ctx);
    const { purchase, payables, request, tasks } = await purchaseProvider.addSellOrder(purchaseId, payablesArray, receivables, invoiceIds, expiresAt);

    // raise event
    const eventData = { purchase, payables, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    // return data
    return {
      payables,
      purchase,
      request,
      tasks,
    };
  }

  @Transaction()
  public async approvePurchase(ctx: Context,
                               requestId: string,
                               rawRequest: string,
                               signedRequest: string,
                               certificate: string,
                               signAlgorithm: string,
                               signMethod: SignMethod) {
    const purchaseProvider = new PurchaseProvider(ctx);
    const { purchase, request, tasks } = await purchaseProvider.approvePurchase(requestId, rawRequest, signedRequest, certificate, signAlgorithm, signMethod);

    // raise event
    const eventData = { purchase, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    return { purchase, request };
  }

  @Transaction()
  public async approvePurchaseByAdmin(ctx: Context, requestId: string, signerId: string, signMethod: SignMethod) {
    const purchaseProvider = new PurchaseProvider(ctx);
    const { purchase, request, tasks } = await purchaseProvider.approvePurchaseByAdmin(requestId, signerId, signMethod);

    // raise event
    const eventData = { purchase, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    return { purchase, request };
  }

  @Transaction()
  public async createPurchaseByThirdParty(ctx: Context,
                                          purchaseId: string,
                                          sellerId: string,
                                          buyerId: string,
                                          expiresAt: string,
                                          payablesString: string,
                                          receivablesString: string,
                                          invoiceIdsString: string,
                                          thirdPartyId: string) {
    // parse arguments
    const payablesData: PayableEntity[] = JSON.parse(payablesString);
    const receivables: IReceivableTerm[] = JSON.parse(receivablesString);
    const invoiceIds: string[] = JSON.parse(invoiceIdsString);

    // create purchase
    const purchaseProvider = new PurchaseProvider(ctx);
    const { purchase, payables } = await purchaseProvider.createPurchaseByThirdParty(purchaseId, sellerId, buyerId, expiresAt, payablesData, receivables, invoiceIds, thirdPartyId);

    // raise event
    const eventData = { purchase, payables };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    return purchase;
  }

  @Transaction()
  public async approveThirdPartyPurchaseByAdmin(ctx: Context, purchaseId: string, approvement: SignMethod) {
    const purchaseProvider = new PurchaseProvider(ctx);
    const purchase = await purchaseProvider.approveThirdPartyPurchaseByAdmin(purchaseId, approvement);

    // raise event
    const eventData = { purchase };
    ContextProvider.raiseEvent(ctx, Configs.events.purchaseUpdate, eventData);

    return purchase;
  }

  @Transaction()
  public async createFundRequest(ctx: Context, fundId: string, payableId: string) {
    const fundProvider = new FundProvider(ctx);
    const { fund, request, tasks } = await fundProvider.createFund(fundId, payableId);

    // raise event
    const eventData = { fund, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.fundUpdate, eventData);

    // return data
    return { fund, request };
  }

  @Transaction()
  public async approveFundRequestByBuyer(ctx: Context,
                                         requestId: string,
                                         rawRequest: string,
                                         signedRequest: string,
                                         certificate: string,
                                         signAlgorithm: string,
                                         approvement: SignMethod): Promise<{ fund: FundEntity, request: MultiSigRequestEntity }> {
    const fundProvider = new FundProvider(ctx);
    const { fund, request, tasks } = await fundProvider.approveFundRequestByBuyer(requestId, rawRequest, signedRequest, certificate, signAlgorithm, approvement);

    // raise event
    const eventData = { fund, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.fundUpdate, eventData);

    // return data
    return { fund, request };
  }

  @Transaction()
  public async approveFundByAdmin(ctx: Context, fundId: string, adminId: string, approvement: SignMethod) {
    const fundProvider = new FundProvider(ctx);
    const fund = await fundProvider.approveFundByAdmin(fundId, adminId, approvement);

    // raise event
    const eventData = { fund };
    ContextProvider.raiseEvent(ctx, Configs.events.fundUpdate, eventData);

    return fund;
  }

  @Transaction()
  public async raiseFund(ctx: Context,
                         fundId: string,
                         transactionId: string,
                         tokenId: string,
                         amountString: string) {
    // parse arguments
    const amount = parseFloat(amountString);

    // raise fund
    const fundProvider = new FundProvider(ctx);
    const { fund, payable, baratTokenTransaction, balance } = await fundProvider.raiseFund(fundId, transactionId, tokenId, amount);

    // raise event
    const eventData = { fund, payable, baratTokenTransaction, balance };
    ContextProvider.raiseEvent(ctx, Configs.events.fundUpdate, eventData);

    // return data
    return { fund, payable, baratTokenTransaction };
  }

  @Transaction()
  public async createPayableSettlement(ctx: Context,
                                       payableSettlementId: string,
                                       payableId: string,
                                       transfersString: string) {
    // parse arguments
    const transfers: PayableSettlementTransfer[] = JSON.parse(transfersString);

    const payableProvider = new PayableProvider(ctx);
    const { payableSettlement, request, tasks } = await payableProvider.createPayableSettlement(payableSettlementId, payableId, transfers);

    // raise event
    const eventData = { payableSettlement, tasks };
    ContextProvider.raiseEvent(ctx, Configs.events.payableSettlementUpate, eventData);

    // return data
    return { payableSettlement, request };
  }

  @Transaction()
  public async approvePayableSettlement(ctx: Context,
                                        requestId: string,
                                        rawRequest: string,
                                        signedRequest: string,
                                        certificate: string,
                                        signAlgorithm: string,
                                        approvement: SignMethod) {
    const payableProvider = new PayableProvider(ctx);
    const {
      payable,
      payableSettlement,
      tokenTransfers,
      requests,
      tasks,
    } = await payableProvider.approvePayableSettlement(requestId,
                                                       rawRequest,
                                                       signedRequest,
                                                       certificate,
                                                       signAlgorithm,
                                                       approvement);

    const eventData = {
      payable,
      payableSettlement,
      tokenTransfers: tokenTransfers || null,
      tasks: tasks || null,
    };
    ContextProvider.raiseEvent(ctx, Configs.events.payableSettlementUpate, eventData);

    // return data
    return {
      payableSettlement,
      requests,
      tokenTransfers: tokenTransfers || null,
    };

  }

  @Transaction()
  public async getTokenSettings(ctx: Context): Promise<BaratTokenSettingsEntity> {
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const tokenSettings = await baratTokenProvider.getBaratTokenSettings();
    return tokenSettings;
  }

  @Transaction()
  public async createTokenSettings(ctx: Context,
                                   priceString: string,
                                   duePeriodString: string,
                                   maxCustomTransactionString: string,
                                   maxTokenTransactionString: string): Promise<BaratTokenSettingsEntity> {
    // parse arguments
    const price = parseInt(priceString, 10);
    const duePeriod = parseInt(duePeriodString, 10);
    const maxCustomTransaction = parseFloat(maxCustomTransactionString);
    const maxTokenTransaction = parseFloat(maxTokenTransactionString);

    // create barat token settings
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const baratTokenSettings = await baratTokenProvider.createBaratTokenSettings(price, duePeriod, maxCustomTransaction, maxTokenTransaction);

    // raise event
    const eventData = { baratTokenSettings };
    await ContextProvider.raiseEvent(ctx, Configs.events.baratTokenSttingsUpdate, eventData);

    // return settings
    return baratTokenSettings;
  }

  @Transaction()
  public async updateTokenSettings(ctx: Context, baratTokenSettingsUpdate: string): Promise<BaratTokenSettingsEntity> {
    // parse arguments
    const baratTokenSettingsData: BaratTokenSettingsEntity = JSON.parse(baratTokenSettingsUpdate);

    // update token settings
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const baratTokenSettings = await baratTokenProvider.updateBaratTokenSettings(baratTokenSettingsData);

    // raise event
    const eventData = { baratTokenSettings };
    await ContextProvider.raiseEvent(ctx, Configs.events.baratTokenSttingsUpdate, eventData);

    return baratTokenSettings;
  }

  @Transaction()
  public async getBaratToken(ctx: Context, tokenId: string) {
    // get token from database
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const token = await baratTokenProvider.getBaratToken(tokenId);

    // return data
    return token;
  }

  @Transaction()
  public async createBaratToken(ctx: Context, tokenId: string, tokenName: string, issuedAt: string) {
    // get token settings from database
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const tokenSettings = await baratTokenProvider.getBaratTokenSettings();
    if (!tokenSettings) {
      throw new Error('Token Settings must be set');
    }

    // create new Token if not exists
    const token = await baratTokenProvider.createBaratToken(tokenId, tokenName, issuedAt);

    // raise event
    const eventData = { token };
    await ContextProvider.raiseEvent(ctx, Configs.events.baratTokenUpdate, eventData);

    // return token
    return token;
  }

  @Transaction()
  public async updateBaratToken(ctx: Context, tokenId: string, tokenName: string, issueAt: string) {
    // update token
    const baratTokenProvider = new BaratTokenProvider(ctx);
    const token = await baratTokenProvider.updateToken(tokenId, tokenName, issueAt);

    // raise event
    const eventData = { token };
    await ContextProvider.raiseEvent(ctx, Configs.events.baratTokenUpdate, eventData);

    // return updatedToken
    return token;
  }

  @Transaction()
  public async removeToken(ctx: Context, tokenId: string) {
    // update token
    const baratTokenProvider = new BaratTokenProvider(ctx);
    await baratTokenProvider.removeToken(tokenId);

    // raise event
    const eventData = { tokenId, removed: true };
    await ContextProvider.raiseEvent(ctx, Configs.events.baratTokenUpdate, eventData);
  }

  @Transaction()
  public async getBalance(ctx: Context, walletId: string, tokenId: string) {
    const baratTokenProvider = new BaratTokenProvider(ctx);
    return await baratTokenProvider.getBalance(walletId, tokenId);
  }

  @Transaction()
  public async getBalances(ctx: Context, walletId: string) {
    const baratTokenProvider = new BaratTokenProvider(ctx);
    return await baratTokenProvider.getBalances(walletId);
  }

  @Transaction()
  public async getTag(ctx: Context, tagId: string): Promise<TagEntity> {
    const tagProvider = new TagProvider(ctx);
    const tag = await tagProvider.getTag(tagId);

    // raise events
    const eventData = { tag };
    ContextProvider.raiseEvent(ctx, Configs.events.tagUpdated, eventData);

    return tag;
  }

  @Transaction()
  public async createTag(ctx: Context, tagId: string, name: string, parentId: string, tagTypeId: string): Promise<TagEntity> {
    const tagProvider = new TagProvider(ctx);
    const tag = await tagProvider.createTag(tagId, name, parentId, tagTypeId);

    // raise events
    const eventData = { created: tag };
    ContextProvider.raiseEvent(ctx, Configs.events.tagUpdated, eventData);

    return tag;
  }

  @Transaction()
  public async updateTag(ctx: Context, tagId: string, tagUpdateString: string): Promise<TagEntity> {
    const tagUpdate = JSON.parse(tagUpdateString) as TagEntity

    const tagProvider = new TagProvider(ctx);
    const tag = await tagProvider.updateTag(tagId, tagUpdate);

    // raise events
    const eventData = { updated: [ tag ] };
    ContextProvider.raiseEvent(ctx, Configs.events.tagUpdated, eventData);

    return tag;
  }

  @Transaction()
  public async removeTag(ctx: Context, tagId: string): Promise<{ removed: TagEntity[] }> {
    const tagProvider = new TagProvider(ctx);
    const { removed } = await tagProvider.removeTag(tagId);

    // raise events
    const eventData = { removed };
    ContextProvider.raiseEvent(ctx, Configs.events.tagUpdated, eventData);

    return { removed };
  }


  @Transaction()
  public async getTagType(ctx: Context, tagTypeId: string): Promise<TagTypeEntity> {
    const tagProvider = new TagProvider(ctx);
    const tagType = await tagProvider.getTagType(tagTypeId);

    // raise events
    const eventData = { tagType };
    ContextProvider.raiseEvent(ctx, Configs.events.tagUpdated, eventData);

    return tagType;
  }

  @Transaction()
  public async createTagType(ctx: Context, tagTypeId: string, name: string, parentId: string): Promise<TagTypeEntity> {
    const tagProvider = new TagProvider(ctx);
    const tagType = await tagProvider.createTagType(tagTypeId, name, parentId);

    // raise events
    const eventData = { created: tagType };
    ContextProvider.raiseEvent(ctx, Configs.events.tagTypeUpdated, eventData);

    return tagType;
  }

  @Transaction()
  public async updateTagType(ctx: Context, tagTypeId: string, tagTypeUpdateString: string): Promise<TagTypeEntity> {
    const tagTypeUpdate = JSON.parse(tagTypeUpdateString) as TagTypeEntity;

    const tagProvider = new TagProvider(ctx);
    const tagType = await tagProvider.updateTagType(tagTypeId, tagTypeUpdate);

    // raise events
    const eventData = { updated: [ tagType ] };
    ContextProvider.raiseEvent(ctx, Configs.events.tagTypeUpdated, eventData);

    return tagType;
  }

  @Transaction()
  public async removeTagType(ctx: Context, tagTypeId: string): Promise<{ removed: TagTypeEntity[] }> {
    const tagProvider = new TagProvider(ctx);
    const { removed } = await tagProvider.removeTagType(tagTypeId);

    // raise events
    const eventData = { removed };
    ContextProvider.raiseEvent(ctx, Configs.events.tagTypeUpdated, eventData);

    return { removed };
  }

}
