import { BaseEntity } from '../core/doc-mangement/base.entity';

export class CaCertEntity extends BaseEntity {
  public certificate: string;
  public collection: string;
}

export interface ICaCert {
  id: string;
  certificate: string;
  createdAt?: string;
  createdBy?: string;
  updatedAt?: string;
}
