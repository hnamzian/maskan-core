import { BaseRepository } from '../core/doc-mangement/base.repository';
import { CaCertEntity } from './ca-cert.entity';

export class CaCertRepository extends BaseRepository<CaCertEntity> {
  protected getKey(caId: string): string {
    return `CA_${caId}`;
  }
}
