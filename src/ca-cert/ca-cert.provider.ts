import { Context } from 'fabric-contract-api';
import { Cryptography } from '../utils/cryptography';
import { CaCertEntity } from './ca-cert.entity';
import { CaCertRepository } from './ca-cert.repository';

export class CaCertificateProvider {
  private caCertRepository: CaCertRepository;

  constructor(ctx: Context) {
    this.caCertRepository = new CaCertRepository(ctx, CaCertEntity);
  }

  public async updateCACertificate(certificate: string): Promise<CaCertEntity> {
    // verify certificate format
    Cryptography.verifyCertificateFormat(certificate);

    // get CA commonName as CaId from certificate
    const { commonName: id } = Cryptography.getCertificateSubject(certificate);

    // create CaCertificate entity object
    const caCertEntity = { certificate, id } as CaCertEntity;

    // create CaCertificate Object
    const caCertificate = await this.caCertRepository.upsert(caCertEntity);

    // return caCertificate object
    return caCertificate;
  }

  public async getCACertificate(caId: string): Promise<CaCertEntity> {
    // get CA certificate from database
    const caCert = await this.caCertRepository.get(caId);

    // return CA certificate
    return caCert;
  }

  public async verifyCertificate(certificate: string): Promise<void> {
    // get certificate issuer
    const { commonName: issuer } = Cryptography.getIssuerSubject(certificate);

    // get ca certificate by issuer of user certificate
    const caCert = await this.getCACertificate(issuer);
    if (!caCert) { throw new Error('Invalid Certificate Issuer'); }

    // verify certifcate against CA certificate
    Cryptography.verifyCertificate(certificate, caCert.certificate);
  }
}
