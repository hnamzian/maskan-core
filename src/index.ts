/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { MaskanCoreChaincode } from './maskan-core.chaincode';
export { MaskanCoreChaincode } from './maskan-core.chaincode';

export const contracts: any[] = [ MaskanCoreChaincode ];
