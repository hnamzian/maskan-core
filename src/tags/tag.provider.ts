import { Context } from 'fabric-contract-api';
import { TagEntity } from './tag.entity';
import { TagRepository } from './tag.repository';
import { TagTypeEntity } from './tag-type.entity';
import { TagTypeRepository } from './tag-type.repository';

export class TagProvider {
  private ctx: Context;
  private tagRepository: TagRepository;
  private tagTypeRepository: TagTypeRepository;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.tagRepository = new TagRepository(this.ctx, TagEntity);
    this.tagTypeRepository = new TagTypeRepository(this.ctx, TagTypeEntity);
  }

  public async getTag(tagId: string): Promise<TagEntity> {
    return this.tagRepository.get(tagId);
  }

  public async createTag(tagId: string, name: string, parentId: string, tagTypeId: string): Promise<TagEntity> {
    if (!name) {
      throw new Error("Tag name must be provided")
    }
    
    // verify parent tag
    let parentTag = null
    if (parentId) {
      parentTag = parentId ? await this.getTag(parentId) : null;
    }
    if (parentId && !parentTag) {
      throw new Error(`Parent Tag with Id: ${parentId} not found`);
    }

    // verify tagType exists
    if (!tagTypeId) {
      throw new Error("Tag Type Id must be provided")
    }
    const tagType = await this.getTagType(tagTypeId)
    if (!tagType) {
      throw new Error(`Tag Type with Id: ${tagTypeId} not found`)
    }

    if (tagType.parentId && parentTag.tagTypeId !== tagType.parentId) {
      throw new Error("Invlaid parent tag or tag type")
    }

    // create new tag
    const tagData = {
      id: tagId,
      name,
      parentId,
      tagTypeId,
    } as TagEntity;
    const tag = await this.tagRepository.create(tagData);

    return tag;
  }

  public async updateTag(tagId: string, tagUpdate: TagEntity): Promise<TagEntity> {
    // get tag by tagId
    const tag = await this.getTag(tagId);

    // throw error if tag does not exist
    if (!tag) {
      throw new Error('Tag with such id not found');
    }

    const updatedTag = tag;
    for (const item in tagUpdate) {
      updatedTag[item] = tagUpdate[item]
    }

    let parentTag = null
    if (updatedTag.parentId) {
      parentTag = await this.getTag(updatedTag.parentId)
      if (!parentTag) {
        throw new Error(`Parent Tag with Id: ${updatedTag.parentId} not found`)
      }
    }

    const tagType = await this.getTagType(updatedTag.tagTypeId)
    if (!tagType) {
      throw new Error(`Tag Type with Id: ${updatedTag.tagTypeId} not found`)
    }

    if (tagType.parentId && parentTag.tagTypeId !== tagType.parentId) {
      throw new Error("Invlaid parent tag or tag type")
    }

    await this.tagRepository.update(updatedTag)

    return updatedTag
  }

  public async removeTag(tagId: string): Promise<{ removed: TagEntity[] }> {
    // get tag by tagId
    const tag = await this.getTag(tagId);

    // throw error if tag does not exist
    if (!tag) {
      throw new Error('Tag with such id not found');
    }

    // remove tag
    await this.tagRepository.delete(tagId);

    // make parentId of all children to null
    let childTags = await this.getChldren(tagId);
    childTags = childTags.map(async (child) => {
      return await this.tagRepository.delete(child.id);
    });

    // return data
    return { 
      removed: [ ...childTags, tag ]
    };
  }

  public async getTagType(tagTypeId: string): Promise<TagTypeEntity> {
    return await this.tagTypeRepository.get(tagTypeId);
  }

  public async createTagType(tagTypeId: string, name: string, parentId: string) {
    // verify parent tag
    const parentTagType = parentId ? await this.getTagType(parentId) : null;
    if (parentId && !parentTagType) {
      throw new Error(`Parent Tag Type with Id: ${parentId} not found`);
    }

    // create new tag
    const tagTypeData = {
      id: tagTypeId,
      name,
      parentId,
    } as TagTypeEntity;
    const tagType = await this.tagTypeRepository.create(tagTypeData);

    return tagType;
  }

  public async updateTagType(tagTypeId: string, tagTypeUpdate: TagTypeEntity): Promise<TagTypeEntity> {
    // get tag by tagId
    const tagType = await this.getTagType(tagTypeId);

    // throw error if tag does not exist
    if (!tagType) {
      throw new Error(`Tag Type with Id: ${tagTypeId} such id not found`);
    }

    let updatedTagType = tagType;
    for (const item in tagTypeUpdate) {
      updatedTagType[item] = tagTypeUpdate[item]
    }

    if (!updatedTagType.name) {
      throw new Error("Invlaid Tag Type name")
    }

    if (updatedTagType.parentId) {
      const parentTagtype = await this.getTagType(updatedTagType.parentId);
      if (!parentTagtype) {
        throw new Error(`Parent Tag Type with Id: ${updatedTagType.parentId} not found`);
      }
    }    

    await this.tagTypeRepository.update(updatedTagType);

    return updatedTagType;
  }

  public async removeTagType(tagTypeId: string): Promise<{ removed: TagTypeEntity[] }> {
    // get tag by tagId
    const tagType = await this.getTagType(tagTypeId);

    // throw error if tag does not exist
    if (!tagType) {
      throw new Error('Tag with such id not found');
    }

    // remove tag
    await this.tagTypeRepository.delete(tagTypeId);

    // make parentId of all children to null
    let childTags = await this.getTagTypeChldren(tagTypeId);
    childTags = childTags.map(async (child) => {
      return await this.tagRepository.delete(child.id);
    });

    // return data
    return { removed: [ ...childTags, tagType ] };
  }

  private async getChldren(tagId: string): Promise<any[]> {
    const query = {
      fields: [ 'id' ],
      selector: {
        parentId: tagId,
      },
    };
    return await this.tagRepository.getByQuery(query);
  }

  private async getTagTypeChldren(tagTypeId: string): Promise<any[]> {
    const query = {
      fields: [ 'id' ],
      selector: {
        parentId: tagTypeId,
      },
    };
    return await this.tagTypeRepository.getByQuery(query);
  }
}
