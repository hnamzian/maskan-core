import { BaseEntity } from '../core/doc-mangement/base.entity';

export class TagTypeEntity extends BaseEntity {
  public id: string;
  public name: string;
  public parentId: string;
  public createdBy: string;
  public createdAt: string;
  public updatedAt: string;
}
