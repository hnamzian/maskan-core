import { BaseEntity } from '../core/doc-mangement/base.entity';

export class TagEntity extends BaseEntity {
  public id: string;
  public name: string;
  public parentId: string;
  public tagTypeId: string;
  public createdBy: string;
  public createdAt: string;
  public updatedAt: string;
}
