import { BaseRepository } from '../core/doc-mangement/base.repository';
import { TagEntity } from './tag.entity';

export class TagRepository extends BaseRepository<TagEntity> {
  protected getKey(tagId: string): string {
    return `TAG_${tagId}`;
  }
}
