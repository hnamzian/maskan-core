import { BaseRepository } from '../core/doc-mangement/base.repository';
import { TagTypeEntity } from './tag-type.entity';

export class TagTypeRepository extends BaseRepository<TagTypeEntity> {
  protected getKey(tagTypeId: string): string {
    return `TAG_TYPE_${tagTypeId}`;
  }
}
