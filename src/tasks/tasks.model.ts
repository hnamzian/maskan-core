import { ExecRequestType } from '../exec-request/exec-request.entity';
import { RequestType } from '../multisig-request/multisig-request.entity';

export class Task {
  public taskType: RequestType | ExecRequestType;
  public taskId: string;
  public walletId: string;
  public personId: string;
  public data: string;
  public status: TaskStatus;
  public createdAt: string;
  public updatedAt: string;

  constructor(task: ITask) {
    for (const prop of Object.keys(task)) {
      this[prop] = task[prop];
    }
  }
}

export interface ITask {
  taskType: RequestType | ExecRequestType;
  taskId: string;
  walletId: string;
  personId: string;
  data: string;
  status: TaskStatus;
  createdAt: string;
  updatedAt: string;
}
export enum TaskStatus {
  todo = 'TODO',
  done = 'DONE',
  notNeeded = 'NOT_NEEDED',
  cancelled = 'CANCELLED',
}
