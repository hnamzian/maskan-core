import { Context } from 'fabric-contract-api';
import { ContextProvider } from '../core/context/context.provider';
import { ExecRequestEntity, ExecRequestStatus } from '../exec-request/exec-request.entity';
import { MultiSigRequestEntity } from '../multisig-request/multisig-request.entity';
import { ITask, Task, TaskStatus } from './tasks.model';

export class TasksProvider {
  private ctx: Context;

  constructor(ctx: Context) {
    this.ctx = ctx;
  }

  public updateTasks(request: MultiSigRequestEntity): Task[] {
    const { id: taskId, requestType: taskType, walletId, createdAt } = request;
    const now = ContextProvider.getTxTimestamp(this.ctx);

    const tasks = request.signers.map((signer) => {
      const taskData = {
        taskId,
        taskType,
        walletId,
        personId: signer,
        data: request.requestData,
        status: TaskStatus.done,
        createdAt,
        updatedAt: now,
      } as ITask;

      if (request.hasEdorsed(signer) || request.hasRejected(signer)) {
        taskData.status = TaskStatus.done;
        return new Task(taskData);
      } else {
        taskData.status = request.isPending()
        ? TaskStatus.todo
        : request.isApproved()
        ? TaskStatus.notNeeded
        : TaskStatus.cancelled;
        return new Task(taskData);
      }
    });

    return tasks;
  }

  public updateExecTasks(request: ExecRequestEntity) {
    const { id: taskId, requestType: taskType, walletId, personId, createdAt } = request;
    const now = ContextProvider.getTxTimestamp(this.ctx);

    const taskStatus = request.status === ExecRequestStatus.created ? TaskStatus.todo : TaskStatus.done;
    const taskData = {
      taskId,
      taskType,
      walletId,
      personId,
      data: request.requestData,
      status: taskStatus,
      createdAt,
      updatedAt: now,
    } as ITask;

    return new Task(taskData);
  }

}
