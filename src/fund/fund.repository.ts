import { BaseRepository } from '../core/doc-mangement/base.repository';
import { FundEntity } from './fund.entity';

export class FundRepository extends BaseRepository<FundEntity> {
  public async getByQuery(filter: object) {
    const query = {
      selector: {
        ...filter,
        collection: this.getCollection(),
      },
    };

    return await super.getByQuery(query);
  }

  protected getKey(fundId: string): string {
    return `FUND_${fundId}`;
  }
}
