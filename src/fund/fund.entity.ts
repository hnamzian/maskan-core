import { BaseEntity } from '../core/doc-mangement/base.entity';

export class FundEntity extends BaseEntity {
  public collection: string;
  public payableId: string;
  public fundReceiverId: string;
  public adminSigner: string;
  public amount: number;
  public status: FundStatus;
  public createdBy: string;
  public createdAt: string;
  public updatedAt: string;

  public isCreated() {
    return this.status === FundStatus.created;
  }
  public isApprovedByBuyer() {
    return this.status === FundStatus.approvedByBuyer;
  }
  public isApproved() {
    return this.status === FundStatus.approved;
  }
  public isRejected() {
    return this.status === FundStatus.rejected;
  }
  public isFunded() {
    return this.status === FundStatus.funded;
  }

}

export enum FundStatus {
  created = 'CREATED',
  approvedByBuyer = 'APPROVED_BY_BUYER',
  approved = 'APPROVED',
  rejected = 'REJECTED',
  funded = 'FUNDED',
}
