import { Context } from 'fabric-contract-api';
import * as _ from 'lodash';
import { BaratBalanceEntity } from '../barat-token/barat-balance.entity';
import { BaratTokenTransactionEntity } from '../barat-token/barat-token-transaction.entity';
import { BaratTokenProvider } from '../barat-token/barat-token.provider';
import { ContextProvider } from '../core/context/context.provider';
import { MultiSigRequestEntity, RequestType, SignMethod } from '../multisig-request/multisig-request.entity';
import { MultiSigRequestProvider } from '../multisig-request/multisig-request.provider';
import { PayableEntity, PayableStatus } from '../payable/payable.entity';
import { PayableProvider } from '../payable/payable.provider';
import { PurchaseProvider } from '../purchase/purchase.provider';
import { Task } from '../tasks/tasks.model';
import { TasksProvider } from '../tasks/tasks.provider';
import { ConsensusType } from '../utils/consensus';
import { WalletProvider } from '../wallet/wallet.provider';
import { FundEntity, FundStatus } from './fund.entity';
import { FundRepository } from './fund.repository';

export class FundProvider {
  private ctx: Context;
  private fundRepository: FundRepository;
  private multiSigRequestService: MultiSigRequestProvider;
  private purchaseService: PurchaseProvider;
  private payableService: PayableProvider;
  private walletService: WalletProvider;
  private tasksService: TasksProvider;
  private baratTokenService: BaratTokenProvider;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.fundRepository = new FundRepository(this.ctx, FundEntity);
    this.multiSigRequestService = new MultiSigRequestProvider(this.ctx);
    this.payableService = new PayableProvider(this.ctx);
    this.purchaseService = new PurchaseProvider(this.ctx);
    this.walletService = new WalletProvider(this.ctx);
    this.tasksService = new TasksProvider(this.ctx);
    this.baratTokenService = new BaratTokenProvider(this.ctx);
  }

  public async getFund(fundId: string): Promise<FundEntity> {
    return await this.fundRepository.get(fundId);
  }

  public async createFund(fundId: string, payableId: string): Promise<{ fund: FundEntity, tasks: Task[], request: MultiSigRequestEntity }> {
    // verify payable exists
    const payable = await this.payableService.getPayable(payableId);
    if (!payable) {
      throw new Error(`Payable with such Id: ${payableId} not found`);
    }

    // verify the purchase is approved
    const purchase = await this.purchaseService.getPurchase(payable.purchaseId);
    if (!purchase.isApproved()) {
      throw new Error(`Purchase with Id: ${purchase.id} is not approved yet`);
    }

    // get fundReceiverId as buyerId of purchase
    const fundReceiverId = purchase.buyerId;

    // if (fundReceiverId !== purchase.buyerId) {
    //   throw new Error(`Wallet with Id: ${fundReceiverId} is not allowed to request fund for purchase with Id: ${purchase.id}`)
    // }

    // verify to not exist another fund for this payable
    const fundQuery = { payableId } as FundEntity;
    const fundsByPayableId = await this.fundRepository.getByQuery(fundQuery);
    if (fundsByPayableId.length > 0) {
      throw new Error(`A fund for payable with Id: ${payableId} has already been requested`);
    }

    // verify purchase status to be approved
    if (!purchase.isApproved()) {
      throw new Error('Purchase Order is not Approved');
    }

    // create fund
    const fundData = {
      id: fundId,
      payableId,
      fundReceiverId,
      amount: null,
      status: FundStatus.created,
    } as FundEntity;
    const fund = await this.fundRepository.create(fundData);

    // create approve fund request
    const request = await this.createApproveFundRequest(fund);

    // create tasks for users
    const tasks = this.tasksService.updateTasks(request);

    // return fund
    return { fund, request, tasks };
  }

  public async approveFundRequestByBuyer(requestId: string,
                                         rawRequest: string,
                                         signedRequest: string,
                                         certificate: string,
                                         signingAlg: string,
                                         signMethod: SignMethod): Promise<{ fund: FundEntity, tasks: Task[], request: MultiSigRequestEntity }> {
    // verify signer to be registered and active
    await this.walletService.verifyWalletByCertificate(certificate);

    // verify corp wallet is still active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }
    await this.walletService.verifyWalletById(request.walletId);

    // Approve request
    request = await this.multiSigRequestService.signRequest(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // parse request data and get fundId
    const { id: fundId } = JSON.parse(rawRequest);

    // retreive fund from database
    let fund = await this.getFund(fundId);

    // update fund status
    if (request.isApproved()) {
      const fundUpdate = {
        id: fundId,
        status: FundStatus.approvedByBuyer,
      } as FundEntity;
      fund = await this.fundRepository.update(fundUpdate);
    } else if (request.isRejected()) {
      fund.status = FundStatus.rejected;
      await this.fundRepository.delete(fund.id);
    }

    // update tasks
    const tasks = this.tasksService.updateTasks(request);

    return { fund, request, tasks };
  }

  public async approveFundByAdmin(fundId: string, adminId: string, approvement: SignMethod): Promise<FundEntity> {
    // get fund and verify fundId exists
    let fund = await this.getFund(fundId);
    if (!fund) {
      throw new Error('Fund Request with such id not found');
    }

    // verify fund status to be APPROVED_BY_BUYER
    if (!fund.isApprovedByBuyer()) {
      throw new Error(`Fund request with Id: ${fundId} has not been approved by buyer`);
    }

    if (approvement === SignMethod.reject) {
      fund.status = FundStatus.rejected;
      await this.fundRepository.delete(fundId);
    } else {
      // update faund
      const fundUpdate = {
        id: fundId,
        adminSigner: adminId,
        status: FundStatus.approved,
      } as FundEntity;
      fund = await this.fundRepository.update(fundUpdate);
    }

    // return fund status
    return fund;
  }

  public async raiseFund(fundId: string,
                         transactionId: string,
                         tokenId: string,
                         amount: number): Promise<{ fund: FundEntity,
                                                    payable: PayableEntity,
                                                    balance: BaratBalanceEntity,
                                                    baratTokenTransaction: BaratTokenTransactionEntity }> {
    // get fund and verify fundId exists
    let fund = await this.getFund(fundId);
    if (!fund) {
      throw new Error(`Fund Request with this Id: ${fundId} not found`);
    }

    // verify fund status to be approved
    if (!fund.isApproved()) {
      throw new Error('Fund cannot be raise at this stage');
    }

    // verify payable no finalized
    let payable = await this.payableService.getPayable(fund.payableId);
    if (payable && payable.isCompleted()) {
      throw new Error(`Payable with Id: ${fund.payableId} has already been finalized.`);
    }

    // update payable status
    // const payableUpdate = {
    //   id: fund.payableId,
    //   status: PayableStatus.completed,
    // } as PayableEntity;
    // payable = await this.payableService.updatePayable(payableUpdate);

    // update fund status
    const fundUpdate = {
      id: fundId,
      status: FundStatus.funded,
      amount,
    } as FundEntity;
    fund = await this.fundRepository.update(fundUpdate);

    // issue token
    const { balance, transaction: baratTokenTransaction } = await this.baratTokenService.issueToken(transactionId, tokenId, fund.fundReceiverId, amount);

    // rturn fund status
    return { fund, payable, balance, baratTokenTransaction };
  }

  private async createApproveFundRequest(fund: FundEntity) {
    if (!fund.isCreated()) {
      throw new Error(`Request for Fund by Id: ${fund.id} cannot be created`);
    }

    const fundReceiverWallet = await this.walletService.getWallet(fund.fundReceiverId);

    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${fund.id}_${now}`;
    const requestData = _.pick(fund, ['id', 'payableId', 'fundReceiverId']);
    const requestDataString = JSON.stringify(requestData);
    return await this.multiSigRequestService.createRequest(requestId,
                                                           fund.fundReceiverId,
                                                           fundReceiverWallet.signingRules,
                                                           ConsensusType.signingRules,
                                                           RequestType.approveFund,
                                                           requestDataString);
  }

}
