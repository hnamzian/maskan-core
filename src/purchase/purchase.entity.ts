import { BaseEntity } from '../core/doc-mangement/base.entity';

export class PurchaseEntity extends BaseEntity {
  public buyerId: string;
  public sellerId: string;
  public order: PurchaseOrder[];
  public invoiceIds: string[];
  public payables: string[];
  public receivableTerms: IReceivableTerm[];
  public thirdPartyCreator: string;
  public expiresAt: string;
  public status: PurchaseStatus;

  public isExpired(now): boolean {
    return this.expiresAt < now;
  }

  public isCreated(): boolean {
    return this.status === PurchaseStatus.createdByOrgs;
  }
  public isApproved(): boolean {
    return this.status === PurchaseStatus.approvedByOrgs || this.status === PurchaseStatus.approvedByThirdParty;
  }
}

export enum PurchaseStatus {
  createdByOrgs = 'CREATED_BY_ORGS',
  sellOrderAddedBySeller = 'SELL_ORDER_ADDED_BY_SELLER',
  approvedBySeller = 'APPROVED_BY_SELLER',
  approvedByOrgs = 'APPROVED_BY_ORGS',
  rejectedByOrgs = 'REJECTED_BY_ORGS',

  sellOrderAddedByThirdParty = 'SELL_ORDER_ADD_BY_THIRD_PARTY',
  approvedByThirdParty = 'APPROVED_BY_ADMIN',
  rejectedByAdmin = 'REJECTED_BY_ADMIN',
}

export interface IReceivableTerm {
  tokenType: string;
  dueDate: number;
}

export interface PurchaseOrder {
  productId: number;
  productName: string;
  quantity: number;
}