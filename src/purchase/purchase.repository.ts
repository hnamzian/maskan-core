import { BaseRepository } from '../core/doc-mangement/base.repository';
import { PurchaseEntity } from './purchase.entity';

export class PurchaseRepository extends BaseRepository<PurchaseEntity> {
  protected getKey(purchaseId: string): string {
    return `PURCHASE_${purchaseId}`;
  }
}
