import { Context } from 'fabric-contract-api';
import * as _ from 'lodash';
import { ContextProvider } from '../core/context/context.provider';
import { ExecRequestEntity, ExecRequestType } from '../exec-request/exec-request.entity';
import { ExecRequestProvider } from '../exec-request/exec-request.provider';
import { MultiSigRequestEntity, RequestType, SignMethod } from '../multisig-request/multisig-request.entity';
import { MultiSigRequestProvider } from '../multisig-request/multisig-request.provider';
import { PayableEntity } from '../payable/payable.entity';
import { PayableProvider } from '../payable/payable.provider';
import { Task } from '../tasks/tasks.model';
import { TasksProvider } from '../tasks/tasks.provider';
import { ConsensusType } from '../utils/consensus';
import { WalletProvider } from '../wallet/wallet.provider';
import { IReceivableTerm, PurchaseEntity, PurchaseOrder, PurchaseStatus } from './purchase.entity';
import { PurchaseRepository } from './purchase.repository';

export class PurchaseProvider {
  private ctx: Context;
  private purchaseRepository: PurchaseRepository;
  private multiSigRequestService: MultiSigRequestProvider;
  private execRequestService: ExecRequestProvider;
  private walletService: WalletProvider;
  private taskService: TasksProvider;

  constructor(ctx) {
    this.ctx = ctx;
    this.purchaseRepository = new PurchaseRepository(this.ctx, PurchaseEntity);
    this.multiSigRequestService = new MultiSigRequestProvider(this.ctx);
    this.execRequestService = new ExecRequestProvider(this.ctx);
    this.walletService = new WalletProvider(this.ctx);
    this.taskService = new TasksProvider(this.ctx);
  }

  public async getPurchase(purchaseId: string): Promise<PurchaseEntity> {
    const purchase = await this.purchaseRepository.get(purchaseId);
    return purchase;
  }

  public async createPurchase(purchaseId: string, sellerId: string, buyerId: string, purchaseOrder: PurchaseOrder[]): Promise<{ purchase: PurchaseEntity, request: ExecRequestEntity, task: Task }> {
    // verify purchasers to create new purchase
    await this.verifyPurchasers(sellerId, buyerId);

    // create purchase
    const purchaseEntity = {
      id: purchaseId,
      sellerId,
      buyerId,
      order: purchaseOrder,
      invoiceIds: [],
      payables: [],
      receivableTerms: [],
      expiresAt: '',
      thirdPartyCreator: null,
      status: PurchaseStatus.createdByOrgs,
    } as PurchaseEntity;
    const purchase = await this.purchaseRepository.create(purchaseEntity);

    // create exec request
    const request = await this.createAddSellOrderRequest(purchase);

    // create task
    const task = await this.taskService.updateExecTasks(request);

    return { purchase, request, task };
  }

  public async addSellOrder(purchaseId: string,
                            payables: PayableEntity[],
                            receivableTerms: IReceivableTerm[],
                            invoiceIds: string[],
                            expiresAt: string): Promise<{ purchase: PurchaseEntity, payables: PayableEntity[], tasks: Task[], request: MultiSigRequestEntity }> {
    let purchase = await this.getPurchase(purchaseId);
    if (!purchase) {
      throw new Error(`Purchase with Id: ${purchase} not found`);
    }
    if (!purchase.isCreated()) {
      throw new Error('Purchase Sell Order is not updateable now');
    }

    // verify purchase expiration
    // const currentTime = ContextProvider.getTxTimestamp(this.ctx);
    // if (purchase.isExpired(currentTime)) {
    //   throw new Error('Purchase has been expired');
    // }

    // update payable status
    const payableService = new PayableProvider(this.ctx);
    payables = await Promise.all(payables.map(async (payable, index) => {
      const payableId = `${purchaseId}_${index}`;
      return await payableService.createPayable(payableId, purchaseId, payable.price, payable.dueDate, payable.description);
    }));

    // update purchase
    const purchaseUpdate = {
      id: purchaseId,
      invoiceIds,
      payables: payables.map((payable) => payable.id),
      receivableTerms,
      expiresAt,
      status: PurchaseStatus.sellOrderAddedBySeller,
    } as PurchaseEntity;
    purchase = await this.purchaseRepository.update(purchaseUpdate);

    // finalize addSellOrder task
    const executerRequest = await this.finalizeAddSellOrderRequest(purchase);
    const executerTask = this.taskService.updateExecTasks(executerRequest);

    // create multi sig request
    const request = await this.createApprovePurchaseRequest(purchase, payables);

    // create seller tasks
    const sellerTasks = this.taskService.updateTasks(request);

    // integrate all tasks
    const tasks = [ ...sellerTasks, executerTask ];

    // return updated purchase
    return { purchase, payables, tasks, request };
  }

  public async approvePurchase(requestId: string,
                               rawRequest: string,
                               signedRequest: string,
                               certificate: string,
                               signingAlg: string,
                               signMethod: SignMethod) {
    // verify signer to be registered and active
    await this.walletService.verifyWalletByCertificate(certificate);

    // verify corp wallet is still active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }
    await this.walletService.verifyWalletById(request.walletId);

    // Approve request
    request = await this.multiSigRequestService.signRequest(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // Update Purchase
    const requestData = JSON.parse(rawRequest);
    const { id: purchaseId } = requestData;

    // get purchase from database
    let purchase = await this.purchaseRepository.get(purchaseId);

    // get nre state for purchase
    const { newState, nextRequest } = this.processPurchaseRquest(purchase, request);
    if (newState) {
      purchase = await this.purchaseRepository.update({ id: purchaseId, status: newState } as PurchaseEntity);
    }

    // create new request
    let newRequest: MultiSigRequestEntity;
    if (nextRequest) {
      const payables = await this.getPayables(purchaseId);
      newRequest = await this.createApprovePurchaseRequest(purchase, payables);
    }

    // create tasks
    const oldTasks = this.taskService.updateTasks(request);
    const tasks = newRequest ? [ ...oldTasks, ...this.taskService.updateTasks(newRequest) ] : oldTasks;

    return { purchase, request: newRequest, tasks };
  }

  public async approvePurchaseByAdmin(requestId: string, signerId: string, signMethod: SignMethod) {
    // verify signer to be registered and active
    await this.walletService.verifyWalletById(signerId);

    // verify corp wallet is still active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }
    await this.walletService.verifyWalletById(request.walletId);

    // Approve request
    request = await this.multiSigRequestService.signRequestByAdmin(requestId, signerId, signMethod);

    // Update Purchase
    const requestData = JSON.parse(request.requestData);
    const { id: purchaseId } = requestData;

    // get purchase from database
    let purchase = await this.purchaseRepository.get(purchaseId);

    // get nre state for purchase
    const { newState, nextRequest } = this.processPurchaseRquest(purchase, request);
    if (newState) {
      purchase = await this.purchaseRepository.update({ id: purchaseId, status: newState } as PurchaseEntity);
    }

    // create new request
    let newRequest: MultiSigRequestEntity;
    if (nextRequest) {
      const payables = await this.getPayables(purchaseId);
      newRequest = await this.createApprovePurchaseRequest(purchase, payables);
    }

    // create tasks
    const oldTasks = this.taskService.updateTasks(request);
    const tasks = newRequest ? [ ...oldTasks, ...this.taskService.updateTasks(newRequest) ] : oldTasks;

    return { purchase, request: newRequest, tasks };
  }

  public async createPurchaseByThirdParty(purchaseId: string,
                                          sellerId: string,
                                          buyerId: string,
                                          expiresAt: string,
                                          payables: PayableEntity[],
                                          receivableTerms: IReceivableTerm[],
                                          invoiceIds: string[],
                                          thirdPartyId: string) {
    // verify expiresAt exists
    if (!expiresAt) {
      throw new Error('Purchase Expiration must be defined');
    }

    // verify expiresAt not to be before current time
    if (expiresAt < ContextProvider.getTxTimestamp(this.ctx)) {
      throw new Error('Purchase Expiration is invalid');
    }

    // verify thirdPartyId arg to be existed
    if (!thirdPartyId) {
      throw new Error('thirdPartyId must be defined');
    }

    // verify sellerId is a registered wallet
    await this.walletService.verifyWalletById(sellerId);

    // verify buyerId is registered wallet
    await this.walletService.verifyWalletById(buyerId);

    // parse arguments
    const payableService = new PayableProvider(this.ctx);
    payables = await Promise.all(payables.map(async (payable, index) => {
      const payableId = `${purchaseId}_${index}`;
      return await payableService.createPayable(payableId, purchaseId, payable.price, payable.dueDate, payable.description);
    }));

    // create new wallet if not exists
    const purchaseUpdate = {
      id: purchaseId,
      sellerId,
      buyerId,
      expiresAt,
      invoiceIds,
      thirdPartyCreator: thirdPartyId,
      payables: payables.map((payable) => payable.id),
      receivableTerms,
      status: PurchaseStatus.sellOrderAddedByThirdParty,
    } as PurchaseEntity;
    const purchase = await this.purchaseRepository.create(purchaseUpdate);

    // return data
    return { purchase, payables };
  }

  public async approveThirdPartyPurchaseByAdmin(purchaseId: string, approvement: SignMethod): Promise<PurchaseEntity> {
    let purchase = await this.getPurchase(purchaseId);
    if (!purchase) {
      throw new Error(`Purchase with Id: ${purchaseId} not found`);
    }

    if (purchase.status !== PurchaseStatus.sellOrderAddedByThirdParty) {
      throw new Error('Purchase cannot be approved');
    }

    const purchaseStatus = approvement === SignMethod.endorse ? PurchaseStatus.approvedByThirdParty : PurchaseStatus.rejectedByAdmin;
    const purchaseUpdate = {
      id: purchaseId,
      status: purchaseStatus,
    } as PurchaseEntity;
    purchase = await this.purchaseRepository.update(purchaseUpdate);

    return purchase;
  }

  private async verifyPurchasers(sellerId: string, buyerId: string): Promise<void> {
    // verify seller wallet to contain a valid executer
    const seller = await this.walletService.getWallet(sellerId);
    if (!seller) {
      throw new Error(`Seller with Id: ${sellerId} not found`);
    }
    if (!seller.isActive()) {
      throw new Error(`Seller with Id: ${sellerId} is not active`);
    }
    if (!seller.executerId) {
      throw new Error(`Seller with Id ${sellerId} should introduce an executer`);
    }

    // verify seller wallet to contain a valid executer
    const buyer = await this.walletService.getWallet(buyerId);
    if (!buyer) {
      throw new Error(`Buyer with Id: ${buyerId} not found`);
    }
    if (!buyer.isActive()) {
      throw new Error(`Buyer with Id: ${buyerId} is not active`);
    }
  }

  private async getPayables(purchaseId: string): Promise<PayableEntity[]> {
    const payableService = new PayableProvider(this.ctx);
    const { payables: payableIds } = await this.getPurchase(purchaseId);
    return await Promise.all(payableIds.map(async (payableId) => {
      return payableService.getPayable(payableId);
    }));
  }

  private processPurchaseRquest(purchase: PurchaseEntity, request: MultiSigRequestEntity) {
    let newState: PurchaseStatus = null;
    let nextRequest: RequestType = null;

    if (request.isApproved()) {
      if (purchase.status === PurchaseStatus.sellOrderAddedBySeller && request.requestType === RequestType.approvePurchaseBySeller) {
        // update purchase status to 'APPROVED_BY_SELLER'
        newState = PurchaseStatus.approvedBySeller;
        nextRequest = RequestType.approvePurchaseByBuyer;
      } else if (purchase.status === PurchaseStatus.approvedBySeller && request.requestType === RequestType.approvePurchaseByBuyer) {
        // update purchase status to 'APPROVED'
        newState = PurchaseStatus.approvedByOrgs;
      }
    } else if (request.isRejected()) {
      // update purchase to rejected
      newState = PurchaseStatus.rejectedByOrgs;
    }

    return { newState, nextRequest };
  }

  private async finalizeAddSellOrderRequest(purchase: PurchaseEntity) {
    const requestId = `${purchase.id}_${purchase.sellerId}_${purchase.createdAt}`;

    const request = await this.execRequestService.finishRequest(requestId);

    return request;
  }

  private async createAddSellOrderRequest(purchase: PurchaseEntity) {
    if (purchase.status !== PurchaseStatus.createdByOrgs) {
      throw new Error(`Reuqest for purchase with Id: ${purchase.id} cannot be created at this stage`);
    }

    const { id: walletId, executerId: personId } = await this.walletService.getWallet(purchase.sellerId);
    const requestId = `${purchase.id}_${walletId}_${purchase.createdAt}`;
    const request = await this.execRequestService.getRequest(requestId);
    if (request) {
      return request;
    }

    const requestData = JSON.stringify(purchase);
    return await this.execRequestService.createRequest(requestId, walletId, personId, ExecRequestType.addSellOrder, requestData);
  }

  private async createApprovePurchaseRequest(purchase: PurchaseEntity, payables: PayableEntity[]) {
    if (purchase.status !== PurchaseStatus.sellOrderAddedBySeller && purchase.status !== PurchaseStatus.approvedBySeller) {
      throw new Error(`Reuqest for purchase with Id: ${purchase.id} cannot be created at this stage`);
    }

    const requestWalletId = purchase.status === PurchaseStatus.sellOrderAddedBySeller
      ? purchase.sellerId
      : purchase.buyerId;

    const requestType = purchase.status === PurchaseStatus.sellOrderAddedBySeller
    ? RequestType.approvePurchaseBySeller
    : RequestType.approvePurchaseByBuyer;

    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${purchase.id}_${requestWalletId}_${now}`;
    const request = await this.multiSigRequestService.getRequest(requestId);
    if (request) {
      return request;
    }

    const requestData = _.pick(purchase, ['id', 'buyerId', 'sellerId', 'order', 'invoiceIds', 'payablesreceivableTerms', 'expiresAt']);
    requestData.payables = payables;

    const requestDataString = JSON.stringify(requestData);

    const { signingRules } = await this.walletService.getWallet(requestWalletId);

    return await this.multiSigRequestService.createRequest(requestId,
                                                           requestWalletId,
                                                           signingRules,
                                                           ConsensusType.signingRules,
                                                           requestType,
                                                           requestDataString);
  }
}
