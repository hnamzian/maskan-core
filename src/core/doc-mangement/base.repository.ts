import { Context } from 'fabric-contract-api';
import { Helper } from '../../utils/helper';
import { ContextProvider } from '../context/context.provider';
import { BaseEntity } from './base.entity';

export class BaseRepository<T extends BaseEntity> {
  private context: Context;

  constructor(ctx: Context, private type: new () => T) {
    this.context = ctx;
  }

  public async exists(id: string): Promise<boolean> {
    if (id.length === 0) {
      throw new Error('Empty identifier is Invalid');
    }

    const key = this.getKey(id);
    const bufferData = await this.context.stub.getState(key);
    return (!!bufferData && bufferData.length > 0);
  }

  public async get(id: string): Promise<T> {
    const key = this.getKey(id);

    const bufferData = await ContextProvider.get(this.context, key);
    if (bufferData.length === 0) {
      return null;
    }
    const document: T = (bufferData.length !== 0 ) ? JSON.parse(bufferData.toString()) : null;

    return this.instanceOf(document);
  }

  public async getByQuery(query: object) {
    const queryString = JSON.stringify(query);
    const result = await this.context.stub.getQueryResult(queryString);
    return await Helper.convertIteratorToArray(result);
  }

  public async create(data: T): Promise<T> {
    const { id: modelId } = data;

    if (!modelId || modelId.length === 0) {
      throw new Error('Empty identifier is Invalid');
    }

    // throw error if wallet with this id already exists
    if (await this.exists(modelId)) {
      throw new Error(`Collection data already exists with Id: ${modelId}`);
    }

    // create wallet object
    const document = this.instanceOf(data);
    document.collection = this.getCollection();
    document.createdBy = ContextProvider.getTxCommonName(this.context);
    document.createdAt = ContextProvider.getTxTimestamp(this.context);
    document.updatedAt = ContextProvider.getTxTimestamp(this.context);

    // put document into db
    const key = this.getKey(modelId);
    await ContextProvider.put(this.context, key, document.toBuffer());

    return document;
  }

  public async update(updatedData: T): Promise<T> {
    const { id: modelId } = updatedData;
    if (modelId.length === 0) {
      throw new Error('Empty identifier is Invalid');
    }

    // throw error if data with this id does not exist
    const document = await this.get(modelId);
    if (!document) { throw new Error(`Document with Id: ${modelId} not found`); }

    // create data collection
    document.init(updatedData);
    document.updatedAt = ContextProvider.getTxTimestamp(this.context);

    // put document into db
    const key = this.getKey(modelId);
    await ContextProvider.put(this.context, key, document.toBuffer());

    return document;
  }

  public async upsert(data: T): Promise<T> {
    const { id: modelId } = data;
    if (modelId.length === 0) {
      throw new Error('Empty identifier is Invalid');
    }

    return await this.get(modelId) ? await this.update(data) : await this.create(data);
  }

  public async delete(id: string): Promise<void> {
    if (id.length === 0) {
      throw new Error('Empty identifier is Invalid');
    }

    if (!await this.exists(id)) {
      throw new Error(`No collection data found with Id: ${id}`);
    }

    const key = this.getKey(id);
    await ContextProvider.delete(this.context, key);
  }

  protected instanceOf(data: T) {
    const instance = new this.type();
    instance.init(data);
    return instance;
  }

  protected getKey(id: string): string {
    return `${BaseEntity.name}_${id}`;
  }
  protected getCollection() {
    return this.constructor.name.split('Repository')[0];
  }
}
