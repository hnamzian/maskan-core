export class BaseEntity {
  public id: string;
  public collection: string;
  public createdAt: string;
  public createdBy: string;
  public updatedAt: string;

  public toString() {
    return JSON.stringify(this);
  }

  public toBuffer(encoding: BufferEncoding = 'utf8') {
    return Buffer.from(this.toString(), encoding);
  }

  public init(data: IBase) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }
}

interface IBase {
  id: string;
  createdAt: string;
  createdBy: string;
  updatedAt: string;
}
