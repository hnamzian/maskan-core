import { Context } from 'fabric-contract-api';
import { Helper } from '../../utils/helper';

export class ContextProvider {
  public static async get(ctx: Context, key: string): Promise<Buffer> {
    return await ctx.stub.getState(key);
  }

  public static async getByQuery(ctx: Context, query: object){
    const queryString = JSON.stringify(query);
    const queryResult = await ctx.stub.getQueryResult(queryString);
    return Helper.convertIteratorToArray(queryResult);
  }

  public static async put(ctx: Context, key, buffer: Buffer): Promise<void> {
    await ctx.stub.putState(key, buffer);
  }

  public static async delete(ctx: Context, key): Promise<void> {
    await ctx.stub.deleteState(key);
  }

  public static getTxCommonName(ctx: Context): string {
    return ctx.clientIdentity.getX509Certificate().subject.commonName;
  }

  public static getTxTimestamp(ctx: Context): string {
    const timestamp = ctx.stub.getTxTimestamp();
    return (timestamp.getSeconds() * 1000 + timestamp.getNanos() / 1000000).toString();
  }

  public static async raiseEvent(ctx: Context, eventName: string, data: object) {
    const buffer = Buffer.from(JSON.stringify(data));
    ctx.stub.setEvent(eventName, buffer);
  }
}
