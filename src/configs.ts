export class Configs {
  public static events = {
    caCertificate: 'CA_CERTIFICATE',
    purchaseUpdate: 'PURCHASE_UPDATE',
    tagUpdated: 'TAG_UPDATED',
    tagTypeUpdated: 'TAG_TYPE_UPDATED',
    walletUpdate: 'WALLET_UPDATE',
    fundUpdate: 'FUND_UPDATE',
    payableSettlementUpate: 'PAYABLE_SETTLEMENT_UPDATE',
    baratTokenUpdate: 'BARAT_TOKEN_UPDATE',
    baratTokenSttingsUpdate: 'BARAT_TOKEN_SETTINGS_UPDATE',
  };

  public static walletDeviceTypes = {
    mobile: 'mobile',
    token: 'token',
  };
}
