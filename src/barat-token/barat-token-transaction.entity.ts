import { BaseEntity } from '../core/doc-mangement/base.entity';

export class BaratTokenTransactionEntity extends BaseEntity {
  public type: TransactionType;
  public tokenId: string;
  public amount: number;
  public from: string;
  public to: string;
  public status: string;
  public createdBy: string;
  public createdAt: string;
  public updatedAt: string;
}

export enum TransactionType {
  issue = 'ISSUE',
  transfer = 'TRANSFER',
  burn = 'BURN',
}

export enum TransferStatus {
  created = 'CREATED',
  approvedBySender = 'APPROVED_BY_SENDER',
  approvedByReceiver = 'APPROVED_BY_RECEIVER',
  approved = 'APPROVED',
}
