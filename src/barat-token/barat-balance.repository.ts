import { Context } from 'fabric-contract-api';
import { ContextProvider } from '../core/context/context.provider';
import { BaratBalanceEntity } from './barat-balance.entity';

export class BaratBalanceRepository {
  private ctx: Context;

  constructor(ctx: Context) {
    this.ctx = ctx;
  }

  public async exists(walletId: string, tokenId: string) {
    const key = this.getKey(walletId, tokenId);
    const balance = await ContextProvider.get(this.ctx, key);
    return (!!balance && balance.length > 0);
  }

  public async get(walletId: string, tokenId: string): Promise<BaratBalanceEntity> {
    const key = this.getKey(walletId, tokenId);
    const balance = await ContextProvider.get(this.ctx, key);
    if (!!balance && balance.length > 0) {
      return JSON.parse(balance.toString());
    }
    return null;
  }

  public async getAll(walletId: string): Promise<BaratBalanceEntity[]> {
    const query = {
      selector: {
        collection: this.getCollection(),
        walletId,
      },
    };
    const balances: BaratBalanceEntity[] = await ContextProvider.getByQuery(this.ctx, query);

    return balances;
  }

  public async create(balanceData: BaratBalanceEntity): Promise<BaratBalanceEntity> {
    const { walletId, tokenId, amount } = balanceData;

    // validate balance data
    if (!walletId) {
      throw new Error('walletId must be provided');
    }
    if (!tokenId) {
      throw new Error('tokenId must be provided');
    }
    if (!amount) {
      throw new Error('amount must be provided');
    }

    if (await this.get(walletId, tokenId)) {
      throw new Error(`Balance record for wallet Id: ${walletId} at tokenId: ${tokenId} already exists`);
    }

    const balance = new BaratBalanceEntity();
    balance.collection = this.getCollection();
    balance.walletId = walletId;
    balance.tokenId = tokenId;
    balance.amount = amount;
    balance.createdAt = ContextProvider.getTxTimestamp(this.ctx);
    balance.createdBy = ContextProvider.getTxCommonName(this.ctx);
    balance.updatedAt = ContextProvider.getTxCommonName(this.ctx);

    const key = this.getKey(walletId, tokenId);
    await ContextProvider.put(this.ctx, key, Buffer.from(JSON.stringify(balance)));

    return balance;
  }

  public async update(balanceData: BaratBalanceEntity): Promise<BaratBalanceEntity> {
    const { walletId, tokenId, amount } = balanceData;
    // validate balance data
    if (!walletId) {
      throw new Error('walletId must be provided');
    }
    if (!tokenId) {
      throw new Error('tokenId must be provided');
    }
    if (!amount) {
      throw new Error('amount must be provided');
    }

    const balance = await this.get(walletId, tokenId);
    if (!balance) {
      throw new Error(`Balance record for wallet Id: ${walletId} at tokenId: ${tokenId} not found`);
    }

    balance.amount = amount;

    const key = this.getKey(walletId, tokenId);
    await ContextProvider.put(this.ctx, key, Buffer.from(JSON.stringify(balance)));

    return balance;
  }

  protected getKey(walletId: string, tokenId: string): string {
    return `BARAT_BALANCE_${walletId}_${tokenId}`;
  }
  protected getCollection() {
    return this.constructor.name.split('Repository')[0];
  }
}
