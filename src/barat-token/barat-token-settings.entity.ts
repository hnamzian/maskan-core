import { BaseEntity } from "../core/doc-mangement/base.entity";

export class BaratTokenSettingsEntity extends BaseEntity {
  public price: number;
  public duePeriod: number;
  public maxCustomTransaction: number;
  public maxTokenTransaction: number;
  public createdAt: string;
  public updatedAt: string;
  public createdBy: string;
}
