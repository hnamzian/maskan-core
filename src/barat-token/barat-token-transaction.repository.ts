import { BaseRepository } from '../core/doc-mangement/base.repository';
import { BaratTokenTransactionEntity } from './barat-token-transaction.entity';

export class BaratTokenTransactionRepository extends BaseRepository<BaratTokenTransactionEntity> {
  public getKey(transactionId: string) {
    return `BARAT_TNX_${transactionId}`;
  }
}
