import { BaseEntity } from '../core/doc-mangement/base.entity';

export class BaratBalanceEntity extends BaseEntity {
  public collection: string;
  public walletId: string;
  public tokenId: string;
  public amount: number;
  public createdAt: string;
  public updatedAt: string;
  public createdBy: string;
}
