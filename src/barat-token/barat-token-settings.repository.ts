import { Context } from 'fabric-contract-api';
import { ContextProvider } from '../core/context/context.provider';
import { BaratTokenSettingsEntity } from './barat-token-settings.entity';

export class BaratTokenSettingsRepository {
  private ctx: Context;

  constructor(ctx: Context) {
    this.ctx = ctx;
  }

  public async exists(): Promise<boolean> {
    const key = this.getKey();
    const settings = await ContextProvider.get(this.ctx, key);
    return (!!settings && settings.length > 0);
  }

  public async get(): Promise<BaratTokenSettingsEntity> {
    const key = this.getKey();
    const settings = await ContextProvider.get(this.ctx, key);
    if (!!settings && settings.length > 0) {
      return JSON.parse(settings.toString());
    }
    return null;
  }

  public async create(tokenSettings: BaratTokenSettingsEntity): Promise<BaratTokenSettingsEntity> {
    if (await this.exists()) {
      throw new Error('Token Settings already exists');
    }

    // create new token
    const settings = new BaratTokenSettingsEntity();
    for (const prop in tokenSettings) {
      settings[prop] = tokenSettings[prop];
    }
    settings.collection = this.getCollection();
    settings.createdAt = ContextProvider.getTxTimestamp(this.ctx);
    settings.updatedAt = ContextProvider.getTxTimestamp(this.ctx);
    settings.createdBy = ContextProvider.getTxCommonName(this.ctx);

    const id = this.getKey();
    await this.ctx.stub.putState(id, Buffer.from(JSON.stringify(settings)));

    return settings;
  }

  public async update(tokenSettings: BaratTokenSettingsEntity): Promise<BaratTokenSettingsEntity> {
    // throw error if wallet with this walletId already exists
    const settings = await this.get();
    if (!settings) {
      throw new Error('Token Settings not found');
    }

    // create token object
    for (const prop in tokenSettings) {
      settings[prop] = tokenSettings[prop];
    }
    settings.updatedAt = ContextProvider.getTxTimestamp(this.ctx);

    // put settings into db
    const key = this.getKey();
    await ContextProvider.put(this.ctx, key, Buffer.from(JSON.stringify(settings)));

    return settings;
  }

  private getKey() {
    return `BARAT_SETTINGS`;
  }
  protected getCollection() {
    return this.constructor.name.split('Repository')[0];
  }
}
