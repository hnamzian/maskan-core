import { BaseEntity } from '../core/doc-mangement/base.entity';

export class BaratTokenEntity extends BaseEntity {
  public collection: string;
  public name: string;
  public price: number;
  public maxCustomTransaction: number;
  public maxTokenTransaction: number;
  public issuedAt: string;
  public duePeriod: number;
  public dueDate: number;
  public createdAt: string;
  public updatedAt: string;
  public createdBy: string;
}
