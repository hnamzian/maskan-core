import { Context } from 'fabric-contract-api';
import { ContextProvider } from '../core/context/context.provider';
import { WalletProvider } from '../wallet/wallet.provider';
import { BaratBalanceEntity } from './barat-balance.entity';
import { BaratBalanceRepository } from './barat-balance.repository';
import { BaratTokenSettingsEntity } from './barat-token-settings.entity';
import { BaratTokenSettingsRepository } from './barat-token-settings.repository';
import { BaratTokenTransactionEntity, TransactionType, TransferStatus } from './barat-token-transaction.entity';
import { BaratTokenTransactionRepository } from './barat-token-transaction.repository';
import { BaratTokenEntity } from './barat-token.entity';
import { BaratTokenRepository } from './barat-token.repository';

export class BaratTokenProvider {
  private ctx: Context;
  private baratTokenRepository: BaratTokenRepository;
  private baratTokenSettingsRepository: BaratTokenSettingsRepository;
  private baratTokenTransactionRepository: BaratTokenTransactionRepository;
  private baratTokenBalanceRepository: BaratBalanceRepository;
  private walletService: WalletProvider;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.baratTokenRepository = new BaratTokenRepository(this.ctx, BaratTokenEntity);
    this.baratTokenSettingsRepository = new BaratTokenSettingsRepository(this.ctx);
    this.baratTokenTransactionRepository = new BaratTokenTransactionRepository(this.ctx, BaratTokenTransactionEntity);
    this.baratTokenBalanceRepository = new BaratBalanceRepository(this.ctx);
    this.walletService = new WalletProvider(this.ctx);
  }

  public async getBaratToken(tokenId: string): Promise<BaratTokenEntity> {
    return await this.baratTokenRepository.get(tokenId);
  }

  public async createBaratToken(tokenId: string, tokenName: string, issuedAt: string): Promise<BaratTokenEntity> {
    // get token settings from database
    const tokenSettings = await this.getBaratTokenSettings();
    if (!tokenSettings) {
      throw new Error('Token Settings must be set');
    }

    // create new Token if not exists
    const baratTokenData = {
      id: tokenId,
      name: tokenName,
      price: tokenSettings.price,
      maxCustomTransaction: tokenSettings.maxCustomTransaction,
      maxTokenTransaction: tokenSettings.maxTokenTransaction,
      issuedAt,
      duePeriod: tokenSettings.duePeriod,
    } as BaratTokenEntity;
    const token = await this.baratTokenRepository.create(baratTokenData);

    // return token
    return token;
  }

  public async updateToken(tokenId: string, tokenName: string, issuedAt: string): Promise<BaratTokenEntity> {
    // verify token exists
    const baratToken = await this.getBaratToken(tokenId);
    if (!baratToken) {
      throw new Error(`Barat token with Id: ${tokenId} not found`);
    }

    // verify token has not been issued
    const now = ContextProvider.getTxTimestamp(this.ctx);
    if (baratToken.issuedAt <= now) {
      throw new Error(`Barat token with Id: ${tokenId} has been issued`);
    }

    // update token
    const tokenUpdate = {
      id: tokenId,
      issuedAt,
      name: tokenName,
    } as BaratTokenEntity;
    const updatedToken = await this.baratTokenRepository.update(tokenUpdate);

    // return updatedToken
    return updatedToken;
  }

  public async removeToken(tokenId: string): Promise<void> {
    // update token
    await this.baratTokenRepository.delete(tokenId);
  }

  public async getBaratTokenSettings(): Promise<BaratTokenSettingsEntity> {
    return await this.baratTokenSettingsRepository.get();
  }

  public async createBaratTokenSettings(price: number,
                                        duePeriod: number,
                                        maxCustomTransaction: number,
                                        maxTokenTransaction: number): Promise<BaratTokenSettingsEntity> {
    if (price < 0) {
      throw new Error('Invalid price value');
    }
    if (duePeriod < 0) {
      throw new Error('Invalud duePeriod value');
    }
    if (maxCustomTransaction < 0) {
      throw new Error('Invalud maxCustomTransaction value');
    }
    if (maxTokenTransaction < 0) {
      throw new Error('Invalud maxTokenTransaction value');
    }

    // create token settings
    const tokenSettings = {
      price,
      duePeriod,
      maxCustomTransaction,
      maxTokenTransaction,
    } as BaratTokenSettingsEntity;
    const baratTokenSettings = await this.baratTokenSettingsRepository.create(tokenSettings);

    // return settings
    return baratTokenSettings;
  }

  public async updateBaratTokenSettings(baratTokenSettingsUpdate: BaratTokenSettingsEntity): Promise<BaratTokenSettingsEntity> {
    const baratTokenSettings = await this.baratTokenSettingsRepository.update(baratTokenSettingsUpdate);
    return baratTokenSettings;
  }

  public async existsBalance(walletId: string, tokenId: string): Promise<boolean> {
    if (await this.getBalance(walletId, tokenId)) {
      return true;
    }
    return false;
  }

  public async getBalance(walletId: string, tokenId: string) {
    return await this.baratTokenBalanceRepository.get(walletId, tokenId);
  }

  public async getBalances(walletId: string) {
    return await this.baratTokenBalanceRepository.getAll(walletId);
  }

  public async setBalance(walletId: string, tokenId: string, amount: number): Promise<BaratBalanceEntity> {
    if (amount < 0) {
      throw new Error('amount value must be greater than 0');
    }

    const balance = await this.getBalance(walletId, tokenId);

    if (balance) {
      balance.amount = amount;
      return await this.baratTokenBalanceRepository.update(balance);
    } else {
      const balanceData = {
        walletId,
        tokenId,
        amount,
      } as BaratBalanceEntity;
      return await this.baratTokenBalanceRepository.create(balanceData);
    }
  }
  public async increaseBalance(walletId: string, tokenId: string, amount: number) {
    const balance = await this.getBalance(walletId, tokenId);
    if (balance) {
      balance.amount += amount;
      return await this.baratTokenBalanceRepository.update(balance);
    } else {
      const balanceData = {
        walletId,
        tokenId,
        amount,
      } as BaratBalanceEntity;
      return await this.baratTokenBalanceRepository.create(balanceData);
    }
  }
  public async decreaseBalance(walletId: string, tokenId: string, amount: number) {
    const balance = await this.getBalance(walletId, tokenId);
    if (balance) {
      if (balance.amount < amount) {
        throw new Error('Insufficient amount to decrease from balance');
      }
      balance.amount -= amount;
      return await this.baratTokenBalanceRepository.update(balance);
    } else {
      const balanceData = {
        walletId,
        tokenId,
        amount,
      } as BaratBalanceEntity;
      return await this.baratTokenBalanceRepository.create(balanceData);
    }
  }

  public async getTransaction(transactionId: string): Promise<BaratTokenTransactionEntity> {
    return await this.baratTokenTransactionRepository.get(transactionId);
  }

  public async createTransaction(transactionId: string,
                                 type: TransactionType,
                                 tokenId: string,
                                 amount: number,
                                 from: string,
                                 to: string) {
    if (await this.getTransaction(transactionId)) {
      throw new Error(`A Barat transaction with Id: ${transactionId} already exists`);
    }
    if (from === to) {
      throw new Error(`Source address must differ from destination address`);
    }

    // create new transaction
    const transactionData = {
      id: transactionId,
      type,
      tokenId,
      amount,
      from,
      to,
      status: TransferStatus.created,
    } as BaratTokenTransactionEntity;
    const transaction = await this.baratTokenTransactionRepository.create(transactionData);

    return transaction;
  }

  public async issueToken(transactionId: string,
                          tokenId: string,
                          receiverId: string,
                          amount: number): Promise<{ balance: BaratBalanceEntity, transaction: BaratTokenTransactionEntity }> {
    // verify token existance
    if (!await this.getBaratToken(tokenId)) {
      throw new Error(`Token with Id: ${tokenId} not found`);
    }

    // verify recipient existance
    const wallet = await this.walletService.getWallet(receiverId);
    if (!wallet) {
      throw new Error('Wallet not found for such recipient');
    }

    // verify receiver wallet
    await this.walletService.verifyWalletById(receiverId);

    // verify amount
    if (amount <= 0) {
      throw new Error('transafer amount must be greater than 0');
    }

    // create transaction data
    const issueTransactionData = {
      id: transactionId,
      type: TransactionType.issue,
      amount,
      from: null,
      to: receiverId,
      status: TransferStatus.approved,
    } as BaratTokenTransactionEntity;
    const issueTransaction = await this.baratTokenTransactionRepository.create(issueTransactionData);

    // update balance
    const balance = await this.existsBalance(receiverId, tokenId)
      ? await this.increaseBalance(receiverId, tokenId, amount)
      : await this.setBalance(receiverId, tokenId, amount);

    return {
      balance,
      transaction: issueTransaction,
    };
  }

  public async transfer(transactionId: string, tokenId: string, from: string, to: string, amount: number)
    : Promise<{ transaction: BaratTokenTransactionEntity, fromBalance: BaratBalanceEntity, toBalance: BaratBalanceEntity }> {
    // verify token existance
    if (!await this.getBaratToken(tokenId)) {
      throw new Error('Token with such id not found');
    }

    // verify recipient existance
    await this.walletService.verifyWalletById(from);

    // verify recipient existance
    await this.walletService.verifyWalletById(to);

    // parse and verify amountString
    if (amount < 0) {
      throw new Error('Invalid value for amount');
    }

    // update balances
    const senderBalance = await this.decreaseBalance(from, tokenId, amount);
    const receiverBalance = await this.increaseBalance(to, tokenId, amount);

    // prepare transaction
    const transferTransactionData = {
      id: transactionId,
      type: TransactionType.transfer,
      amount,
      from,
      to,
      status: TransferStatus.approved,
    } as BaratTokenTransactionEntity;
    const transferTransaction = await this.baratTokenTransactionRepository.create(transferTransactionData);

    // return transaction
    return {
      fromBalance: senderBalance,
      toBalance: receiverBalance,
      transaction: transferTransaction,
    };

  }

}
