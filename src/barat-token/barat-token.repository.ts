import { BaseRepository } from '../core/doc-mangement/base.repository';
import { BaratTokenEntity } from './barat-token.entity';

export class BaratTokenRepository extends BaseRepository<BaratTokenEntity> {
  protected getKey(tokenId: string): string {
    return `BARAT_${tokenId}`;
  }
}
