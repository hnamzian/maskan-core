import { BaseEntity } from '../core/doc-mangement/base.entity';
import { SigningRules } from '../utils/SigningRules';

export class WalletEntity extends BaseEntity {
  public type: WalletType;
  public status: WalletStatus;
  public signingRules: object[];
  public corpWallets: string[];
  public executerId: string;
  public tags: string[];
  public createdAt: string;
  public createdBy: string;
  public updatedAt: string;

  public toString() {
    return JSON.stringify(this);
  }

  public toBuffer() {
    return Buffer.from(this.toString());
  }

  get signers(): string[] {
    return SigningRules.getSigners(this.signingRules);
  }

  public isActive() {
    return this.status === WalletStatus.activated;
  }

  public isSigner(userId: string) {
    return this.signers.includes(userId);
  }

  public isExecuter(userId: string) {
    return this.executerId === userId;
  }

  public pushCorpWallets(walletId: string) {
    this.corpWallets.push(walletId);
  }

}

export const walletUpdateableFields = ['executerId', 'signingRules'];

export enum WalletType {
  real = 'REAL',
  corp = 'CORP',
}

export enum WalletStatus {
  activated = 'ACTIVATED',
  pending = 'PENDING',
  suspended = 'SUSPENDED',
}

export interface IWallet {
  walletId?: string;
  walletType?: WalletType;
  status?: WalletStatus;
  signingRules?: object[];
  corpWallets?: string[];
  executerId?: string;
  createdAt?: string;
  createdBy?: string;
  updatedAt?: string;
}
