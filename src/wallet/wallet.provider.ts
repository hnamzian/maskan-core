import { Context } from 'fabric-contract-api';
import * as _ from 'lodash';
import { CaCertificateProvider } from '../ca-cert/ca-cert.provider';
import { ContextProvider } from '../core/context/context.provider';
import { MultiSigRequestEntity, RequestType, SignMethod } from '../multisig-request/multisig-request.entity';
import { MultiSigRequestProvider } from '../multisig-request/multisig-request.provider';
import { TagProvider } from '../tags/tag.provider';
import { Task } from '../tasks/tasks.model';
import { TasksProvider } from '../tasks/tasks.provider';
import { ConsensusType } from '../utils/consensus';
import { Cryptography } from '../utils/cryptography';
import { SigningRules } from '../utils/SigningRules';
import { IWallet, WalletEntity, WalletStatus, WalletType, walletUpdateableFields } from './wallet.entity';
import { WalletRepository } from './wallet.repository';
import { WalletUtils } from './wallet.utils';

export class WalletProvider {
  private walletRepository: WalletRepository;
  private caCertificateService: CaCertificateProvider;
  private multiSigRequestService: MultiSigRequestProvider;
  private taskService: TasksProvider;
  private ctx: Context;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.walletRepository = new WalletRepository(this.ctx, WalletEntity);
    this.caCertificateService = new CaCertificateProvider(this.ctx);
    this.multiSigRequestService = new MultiSigRequestProvider(this.ctx);
    this.taskService = new TasksProvider(this.ctx);
  }

  public async getWallet(typedWalletId: string): Promise<WalletEntity | null> {
    // parse walletId
    const { walletId } = WalletUtils.parseWalletId(typedWalletId);

    // get wallet from database
    const wallet = await this.walletRepository.get(walletId);
    return wallet;
  }

  public async getWalletsByIds(walletIds: string[]): Promise<WalletEntity[]> {
    const wallets = [];

    for (const walletId of walletIds) {
      const corpWallet = await this.walletRepository.get(walletId);
      wallets.push(corpWallet);
    }
    return wallets;
  }

  public async getConnectedWallets(typedWalletId: string): Promise<{ wallet: WalletEntity, corpWallets: WalletEntity[] }> {
    const wallet = await this.getWallet(typedWalletId);

    if (!wallet) {
      throw new Error(`Wallet with Id: ${typedWalletId} not found`);
    }

    const corpWallets: WalletEntity[] = wallet.corpWallets.length > 0
      ? await this.getWalletsByIds(wallet.corpWallets)
      : [];

    return {
      corpWallets,
      wallet,
    };
  }

  public async verifyWalletById(walletId: string): Promise<WalletEntity> {
    const wallet = await this.getWallet(walletId);
    if (!wallet) {
      throw new Error(`Wallet with Id: ${walletId} has not registered`);
    }

    if (wallet.status !== WalletStatus.activated) {
      throw new Error(`Wallet with Id: ${walletId} is not Active`);
    }

    return wallet;
  }

  public async verifyWalletByCertificate(certificate: string): Promise<WalletEntity> {
    // verify certifcate against CA certificate
    await this.caCertificateService.verifyCertificate(certificate);

    // exteract walletId from certficate and verify wallet by id
    const { commonName: walletId } = Cryptography.getCertificateSubject(certificate);
    const wallet = await this.verifyWalletById(walletId);

    return wallet;
  }

  public async createRealWallet(typedWalletId: string, corpWallets: string[] = []): Promise<{ wallet: WalletEntity, tasks: Task[], request: MultiSigRequestEntity }> {
    // parse typedWalletId
    const parsedWalletId = WalletUtils.parseWalletId(typedWalletId);
    const walletId = parsedWalletId.walletId;

    // create wallet object
    const walletType = WalletType.real;
    const signingRules = [
      {
        and: {
          signers: [ walletId ],
        },
      },
    ];
    const executerId = walletId;
    const status: WalletStatus = WalletStatus.pending;

    // store wallet in database
    const walletEntity = {
      id: walletId,
      corpWallets,
      type: walletType,
      signingRules,
      executerId,
      tags: [],
      status,
    } as WalletEntity;
    const wallet = await this.walletRepository.create(walletEntity);

    // Create request to approve wallet
    const request = await this.createApproveWalletRequest(wallet);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return {
      request,
      tasks,
      wallet,
    };
  }

  public async createCorpWallet(walletId: string,
                                signingRules: object[] = [],
                                executerId: string = ''): Promise<{wallet: WalletEntity, tasks: Task[], request: MultiSigRequestEntity}> {
    // verify signers are all registered
    const signers = SigningRules.getSigners(signingRules);
    for (const signer of signers) {
      await this.verifyWalletById(signer);
    }

    // create corporate wallet
    const walletEntity = {
      id: walletId,
      corpWallets: [],
      type: WalletType.corp,
      signingRules,
      executerId,
      tags: [],
      status: WalletStatus.pending,
    } as WalletEntity;
    const wallet = await this.walletRepository.create(walletEntity);

    // create real wallets for signers have not registered &&
    // add corp walletId to signers' corpWallets list
    const signerWallets = await this.attachCorpToSignerWallets(wallet);

    // create requests for all signers to approve corp wallet
    const request = await this.createApproveWalletRequest(wallet);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return {
      request,
      tasks,
      wallet,
    };
  }

  public async updateWallet(walletId: string, walletUpdate: IWallet): Promise<WalletEntity> {
    if (!this.validateWalletUpdate(walletUpdate)) {
      throw new Error('Invalid fields to update');
    }

    const walletUpdateEntity = {
      id: walletId,
      ...walletUpdate,
    } as WalletEntity;
    const updatedWallet = await this.walletRepository.update(walletUpdateEntity);

    return updatedWallet;
  }

  public async approveWalletByAdmin(requestId: string, signerId: string, signMethod: SignMethod) {
    // parse signerId to remove postfix
    signerId = WalletUtils.parseWalletId(signerId).walletId;

    // verify signer wallet is active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    let wallet = await this.getWallet(request.walletId);
    if (wallet.type === WalletType.corp) {
      await this.verifyWalletById(signerId);
    }

    // approve request by Admin on-behalf of user
    request = await this.multiSigRequestService.signRequestByAdmin(requestId, signerId, signMethod);

    // verify endorsemnets and update wallet status when reached to consensus
    const status = request.isApproved()
      ? WalletStatus.activated
      : request.isRejected()
      ? WalletStatus.suspended
      : WalletStatus.pending;

    const walletUpdate = { id: request.walletId, status } as WalletEntity;
    wallet = await this.walletRepository.update(walletUpdate);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return { wallet, tasks, request };
  }

  public async approveWlletByMember(requestId: string,
                                    rawRequest: string,
                                    signedRequest: string,
                                    certificate: string,
                                    signingAlg: string,
                                    signMethod: SignMethod) {
    // verify signer wallet to be registered and active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    let wallet = await this.getWallet(request.walletId);
    if (wallet.type === WalletType.corp) {
      await this.verifyWalletByCertificate(certificate);
    }

    // Approve request
    request = await this.multiSigRequestService.signRequest(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // Update Wallet status
    const status = request.isApproved()
      ? WalletStatus.activated
      : request.isRejected()
      ? WalletStatus.suspended
      : WalletStatus.pending;

    const walletUpdate = { id: request.walletId, status } as WalletEntity;
    wallet = await this.walletRepository.update(walletUpdate);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return { wallet, tasks, request };
  }

  public async createWalletUpdate(walletId: string, walletUpdate: WalletEntity) {
    if (!this.validateWalletUpdate(walletUpdate)) {
      throw new Error('Some fields are nor updateable');
    }
    walletUpdate.id = walletId;

    let wallet = await this.getWallet(walletId);
    if (!wallet) {
      throw new Error(`Wallet with Id: ${walletId} not found`);
    }

    const walletUpdateString = JSON.stringify(walletUpdate);
    const request = await this.createApproveWalletUpdateRequest(wallet, walletUpdateString);

    wallet = await this.walletRepository.update({
      id: wallet.id,
      status: WalletStatus.pending,
    } as WalletEntity);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return { wallet, tasks, request };
  }

  public async approveWalletUpdateByAdmin(requestId: string, signerId: string, signMethod: SignMethod) {
    // Approve Request
    signerId = WalletUtils.parseWalletId(signerId).walletId;
    const request = await this.multiSigRequestService.signRequestByAdmin(requestId, signerId, signMethod);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    let wallet = await this.getWallet(request.walletId);
    if (wallet.type === WalletType.corp) {
      await this.verifyWalletById(signerId);
    }

    // Update Request
    wallet = request.isApproved()
      ? await this.walletRepository.update({
        id: request.walletId,
        status: WalletStatus.activated,
        ...JSON.parse(request.requestData),
      } as WalletEntity)
      : request.isRejected()
      ? await this.walletRepository.update({
          id: request.walletId,
          status: WalletStatus.activated,
        } as WalletEntity)
      : await this.getWallet(request.walletId);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return { wallet, tasks, request };
  }

  public async approveWalletUpdateByMember(requestId: string,
                                           rawRequest: string,
                                           signedRequest: string,
                                           certificate: string,
                                           signingAlg: string,
                                           signMethod: SignMethod) {
    // verify signer wallet to be registered and active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    let wallet = await this.getWallet(request.walletId);
    if (wallet.type === WalletType.corp) {
      await this.verifyWalletByCertificate(certificate);
    }

    // Approve request
    request = await this.multiSigRequestService.signRequest(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // Update Request
    wallet = request.isApproved()
      ? await this.walletRepository.update({
        id: request.walletId,
        status: WalletStatus.activated,
        ...JSON.parse(request.requestData),
      } as WalletEntity)
      : request.isRejected()
      ? await this.walletRepository.update({
          id: request.walletId,
          status: WalletStatus.activated,
        } as WalletEntity)
      : await this.getWallet(request.walletId);

    // create tasks
    const tasks = this.taskService.updateTasks(request);

    return { wallet, tasks, request };
  }

  public async updateTags(walletId: string, tagIds: string[]) {
    // verify tagIds to be an array
    if (tagIds.length === 0) {
      throw new Error('No tags provided to update wallet tags');
    }

    // get wallet and verify wallet exists
    const wallet = await this.getWallet(walletId);
    if(!wallet) {
      throw new Error(`Wallet with Id: ${walletId} not found`);
    }

    // verify tagIds are all exists
    const tagService = new TagProvider(this.ctx);
    for (const tag of tagIds) {
      if (! await tagService.getTag(tag)) {
        throw new Error(`Tag with Id: ${tag} not found`);
      }
    }

    // update wallet tags
    wallet.tags = tagIds;
    await this.walletRepository.update(wallet);

    return wallet;
  }
  public async addTags(walletId: string, tagIds: string[]) {
    // verify tagIds to be an array
    if (tagIds.length === 0) {
      throw new Error('No tags provided to update wallet tags');
    }

    // get wallet and verify wallet exists
    const wallet = await this.getWallet(walletId);
    if(!wallet) {
      throw new Error(`Wallet with Id: ${walletId} not found`);
    }

    // verify tagIds are all exists
    const tagService = new TagProvider(this.ctx);
    for (const tag of tagIds) {
      if (! await tagService.getTag(tag)) {
        throw new Error(`Tag with Id: ${tag} not found`);
      }
    }

    // add tags to wallet
    let walletTags = wallet.tags;
    for (const tag of tagIds) {
      walletTags = walletTags.includes(tag) ? [ ...walletTags, tag ] : walletTags;
    }
    wallet.tags = walletTags;
    await this.walletRepository.update(wallet);

    return wallet;
  }
  public async removeTags(walletId: string, tagIds: string[]) {
    // verify tagIds to be an array
    if (tagIds.length === 0) {
      throw new Error('No tags provided to remove wallet tags');
    }

    // get wallet and verify wallet exists
    const wallet = await this.getWallet(walletId);
    if(!wallet) {
      throw new Error(`Wallet with Id: ${walletId} not found`);
    }

    // verify tagIds are all exists
    const tagService = new TagProvider(this.ctx);
    for (const tag of tagIds) {
      if (! await tagService.getTag(tag)) {
        throw new Error(`Tag with Id: ${tag} not found`);
      }
    }

    // add tags to wallet
    let walletTags = wallet.tags;
    walletTags = walletTags.filter((walletTag) => !tagIds.includes(walletTag));
    await this.walletRepository.update(wallet);

    return wallet;
  }

  private async createApproveWalletRequest(wallet: WalletEntity) {
    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${wallet.id}_${now}`;
    const requestData = _.pick(wallet, ['id', 'signingRules', 'createdAt', 'executerId', 'type']);
    const requestDataString = JSON.stringify(requestData);
    return await this.multiSigRequestService.createRequest(requestId,
                                                            wallet.id,
                                                            wallet.signingRules,
                                                            ConsensusType.andOfAll,
                                                            RequestType.approveWallet,
                                                            requestDataString);
  }

  private async createApproveWalletUpdateRequest(wallet: WalletEntity, walletUpdateString: string) {
    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${wallet.id}_${now}`;
    return await this.multiSigRequestService.createRequest(requestId,
                                                           wallet.id,
                                                           wallet.signingRules,
                                                           ConsensusType.andOfAll,
                                                           RequestType.approveWalletUpdate,
                                                           walletUpdateString);
  }

  private validateWalletUpdate(walletUpdate: IWallet): boolean {
    return Object.keys(walletUpdate).every((key) => walletUpdateableFields.includes(key));
  }

  private async attachCorpToSignerWallets(corpWallet: WalletEntity): Promise<WalletEntity[]> {
    const signers = corpWallet.signers;
    if (signers.length === 0) {
      return [];
    }

    const signerWallets = [] as WalletEntity[];
    for (const signer of signers) {
      const wallet = await this.getWallet(signer);
      if (!wallet) {
        const { wallet: signerWallet } = await this.createRealWallet(signer, [ corpWallet.id ]);
        signerWallets.push(signerWallet);
      } else {
        wallet.pushCorpWallets(corpWallet.id);
        const walletUpdate = {
          corpWallets: wallet.corpWallets,
          id: signer,
        } as WalletEntity;
        await this.walletRepository.update(walletUpdate);
      }
    }

    return signerWallets;
  }

}
