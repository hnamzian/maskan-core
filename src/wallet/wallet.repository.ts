import { BaseRepository } from '../core/doc-mangement/base.repository';
import { WalletEntity } from './wallet.entity';

export class WalletRepository extends BaseRepository<WalletEntity> {
  protected getKey(walletId: string): string {
    return `WALLET_${walletId}`;
  }
}
