import { Configs } from '../configs';

export class WalletUtils {
  public static parseWalletId(typedWalletId) {
    const parsed = typedWalletId.split('_');
    const walletId = parsed[0];
    const device = parsed[1] ? parsed[1] : null;

    if (device &&
        device !== Configs.walletDeviceTypes.mobile &&
        device !== Configs.walletDeviceTypes.token) { throw new Error('Invalid Wallet Device type'); }

    if (device) {
      return {
        type: parsed[1],
        walletId,
      };
    }

    return {
      walletId,
    };
  }
}
