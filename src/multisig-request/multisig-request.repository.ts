import { BaseRepository } from '../core/doc-mangement/base.repository';
import { MultiSigRequestEntity } from './multisig-request.entity';

export class MultiSigRequestRepository extends BaseRepository<MultiSigRequestEntity> {
  protected getKey(requestId: string): string {
    return `MSGRQ_${requestId}`;
  }
}
