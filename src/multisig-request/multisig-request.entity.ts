import { BaseEntity } from '../core/doc-mangement/base.entity';
import { ConsensusStatus, ConsensusType } from '../utils/consensus';
import { SigningRules } from '../utils/SigningRules';

export class MultiSigRequestEntity extends BaseEntity {
  public walletId: string;
  public signingRules: object[];
  public consensus: ConsensusType;
  public endorsers: string[];
  public rejectors: string[];
  public requestData: string;
  public requestType: RequestType;
  public status: ConsensusStatus;
  public createdAt: string;
  public updatedAt: string;

  public toString() {
    return JSON.stringify(this);
  }

  get signers() {
    return SigningRules.getSigners(this.signingRules);
  }

  public toBuffer() {
    return Buffer.from(this.toString());
  }

  public pushEndorser(userId: string) {
    if (!this.hasEdorsed(userId)) {
      this.endorsers.push(userId);
    }
  }

  public pushRejectors(userId: string) {
    if (!this.hasRejected(userId)) {
      this.rejectors.push(userId);
    }
  }

  public canSign(signer) {
    return this.signers.includes(signer);
  }

  public hasEdorsed(userId: string): boolean {
    return this.endorsers.includes(userId);
  }
  public hasRejected(userId: string): boolean {
    return this.rejectors.includes(userId);
  }

  public isApproved(): boolean {
    return this.status === ConsensusStatus.approved;
  }
  public isRejected(): boolean {
    return this.status === ConsensusStatus.rejected;
  }
  public isPending(): boolean {
    return this.status === ConsensusStatus.pending;
  }

}

export interface IMultiSigRequest {
  requestId?: string;
  walletId?: string;
  signingRules?: object[];
  endorsers?: string[];
  rejectors?: string[];
  requestData?: string;
  requestType?: RequestType;
  status?: ConsensusStatus;
  createdAt?: string;
  updatedAt?: string;
}

export enum SignMethod {
  endorse = 'ENDORSE',
  reject = 'REJECT',
}

export enum RequestType {
  approveWallet = 'APPROVE_WALLET',
  approveWalletUpdate = 'APPROVE_WALLET_UPDATE',

  addSellOrder = 'ADD_SELL_ORDER',
  approvePurchaseBySeller = 'APPROVE_PURCHASE_BY_SELLER',
  approvePurchaseByBuyer = 'APPROVE_PURCHASE_BY_BUYER',

  approveFund = 'APPROVE_FUND',

  approveSettlementBySender = 'APPROVE_SETTLEMENT_BY_SENDER',
  approveSettlementByReceiver = 'APPROVE_SETTLEMENT_BY_RECEIVER',
}
