import { Context } from 'fabric-contract-api';
import { CaCertificateProvider } from '../ca-cert/ca-cert.provider';
import { Consensus, ConsensusStatus, ConsensusType } from '../utils/consensus';
import { Cryptography } from '../utils/cryptography';
import { WalletUtils } from '../wallet/wallet.utils';
import { MultiSigRequestEntity, RequestType, SignMethod } from './multisig-request.entity';
import { MultiSigRequestRepository } from './multisig-request.repository';

export class MultiSigRequestProvider {
  private multiSigRequestRepository: MultiSigRequestRepository;
  private caCertificateService: CaCertificateProvider;
  private ctx: Context;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.multiSigRequestRepository = new MultiSigRequestRepository(this.ctx, MultiSigRequestEntity);
    this.caCertificateService = new CaCertificateProvider(this.ctx);
  }

  public async getRequest(requestId: string) {
    const request = await this.multiSigRequestRepository.get(requestId);
    return request;
  }

  public async createRequest(requestId: string,
                             walletId: string,
                             signingRules: object,
                             consensus: ConsensusType,
                             requestType: RequestType,
                             requestData: string) {
    // Create multisig request data
    const requestObject = {
      id: requestId,
      walletId,
      signingRules,
      consensus,
      endorsers: [],
      rejectors: [],
      requestData,
      requestType,
      status: ConsensusStatus.pending,
    } as MultiSigRequestEntity;

    // Store request to database
    const request = await this.multiSigRequestRepository.create(requestObject);

    return request;
  }

  public async signRequest(requestId: string,
                           requestData: string,
                           signedRequest: string,
                           certificate: string,
                           signingAlgorithm: string,
                           signMethod: SignMethod) {
    // verify signature
    Cryptography.verifySignature(certificate, requestData, signedRequest, signingAlgorithm);

    // get request from requestId
    const request = await this.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    // verify request data signed with one stored in database
    if (requestData !== request.requestData) {
      throw new Error(`Mismath in request data signed`);
    }

    // Verify user certificate against root CA
    await this.caCertificateService.verifyCertificate(certificate);

    // get signerId from certificate subject
    const { commonName: signerCn } = Cryptography.getCertificateSubject(certificate);
    const { walletId: signerId } = WalletUtils.parseWalletId(signerCn);

    // Verify Request is not concluded
    // if (request.isApproved() || request.isRejected()) {
    //   throw new Error(`The request with Id: ${request.id} has been Concluded before`);
    // }

    if (!request.canSign(signerId)) {
      throw new Error(`Permission denied: User with Id: ${signerId} does not have permission to sign this request`);
    }

    // verify the user has not endorsed/rejected this request
    if (request.hasEdorsed(signerId) || request.hasRejected(signerId)) {
      throw new Error(`Request with Id ${request.id} has been signed by this user: ${signerId}`);
    }

    // update request
    let requestUpdate = this.applySignMethod(request, signerId, signMethod);
    if (requestUpdate.isApproved() || requestUpdate.isRejected()) {
      await this.multiSigRequestRepository.delete(requestId);
    } else {
      requestUpdate = await this.multiSigRequestRepository.update(requestUpdate);
    }
    return requestUpdate;
  }

  public async signRequestByAdmin(requestId: string, signerId: string, signMethod: SignMethod): Promise<MultiSigRequestEntity> {
    // get request from requestId
    const request = await this.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    // Verify Request is not concluded
    if (request.isApproved() || request.isRejected()) {
      throw new Error(`The request with Id: ${request.id} has been Concluded before`);
    }

    if (!request.canSign(signerId)) {
      throw new Error(`Permission denied: User with Id: ${signerId} does not have permission to sign this request`);
    }

    // verify the user has not endorsed/rejected this request
    if (request.hasEdorsed(signerId) || request.hasRejected(signerId)) {
      throw new Error(`Request with Id ${request.id} has been signed by this user: ${signerId}`);
    }

    // update request
    let requestUpdate = this.applySignMethod(request, signerId, signMethod);
    if (requestUpdate.isApproved() || requestUpdate.isRejected()) {
      await this.multiSigRequestRepository.delete(requestId);
    } else {
      requestUpdate = await this.multiSigRequestRepository.update(requestUpdate);
    }
    return requestUpdate;
  }

  private applySignMethod(request: MultiSigRequestEntity, signer: string, signMethod: SignMethod): MultiSigRequestEntity {
    return signMethod === SignMethod.endorse
      ? this.applyEndorement(request, signer)
      : this.applyRejection(request, signer);
  }
  private applyEndorement(request: MultiSigRequestEntity, signer: string): MultiSigRequestEntity {
    request.pushEndorser(signer);
    const consensus = new Consensus(request.signingRules, request.consensus);
    request.status = consensus.verifyConsensus(request.endorsers, request.rejectors);
    return request;
  }
  private applyRejection(request: MultiSigRequestEntity, signer: string): MultiSigRequestEntity {
    request.pushRejectors(signer);
    const consensus = new Consensus(request.signingRules, request.consensus);
    request.status = consensus.verifyConsensus(request.endorsers, request.rejectors);
    return request;
  }

}
