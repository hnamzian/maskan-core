import { Context, Contract, Info } from 'fabric-contract-api';

@Info({ title: 'Base Contract', description: 'Base COntract' })
export class BaseChaincode extends Contract {
  public async beforeTransaction(ctx: Context) {
    // Log incocation params
    console.log(`<<<<Call Chaincode Methode>>>>`);
    const { fcn, params } = ctx.stub.getFunctionAndParameters();
    console.log(`Methode Name: ${fcn}`);
    console.log(`Parameters: ${params}`);
    console.log(`TxID: ${ctx.stub.getTxID()}`);
  }
}
