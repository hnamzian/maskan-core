import { SigningRules } from './SigningRules';

export class Consensus {
  private signingRules: object[];
  private consensus: ConsensusType;

  constructor(signingRules: object[], consensus: ConsensusType) {
    this.signingRules = signingRules;
    this.consensus = consensus;
  }

  public verifyConsensus(endorsers: string[], rejectors: string[]): ConsensusStatus {
    if (this.consensus === ConsensusType.andOfAll) {
      return this.verifyConsensusALL(endorsers, rejectors, this.signingRules);
    } else if (this.consensus === ConsensusType.signingRules) {
      return this.verifyConsensusDefault(endorsers, rejectors, this.signingRules);
    } else {
      throw new Error(`Invalid Consensus method: ${this.consensus}`);
    }
  }

  private verifyConsensusDefault(endorsers, rejectors, signingRules): ConsensusStatus {
    return SigningRules.verifySigningRule(endorsers, signingRules)
      ? ConsensusStatus.approved
      : SigningRules.verifySigningRule(rejectors, signingRules)
      ? ConsensusStatus.rejected
      : ConsensusStatus.pending;
  }
  private verifyConsensusALL(endorsers, rejectors, signingRules): ConsensusStatus {
    const signers = SigningRules.getSigners(signingRules);

    return rejectors.length > 0
      ? ConsensusStatus.rejected
      : signers.every((s) => endorsers.includes(s))
      ? ConsensusStatus.approved
      : ConsensusStatus.pending;
  }
}

export enum ConsensusType {
  andOfAll = 'AND_OF_ALL',
  signingRules = 'SIGNING_RULES',
}

export enum ConsensusStatus {
  pending = 'PENDING',
  approved = 'APPROVED',
  rejected = 'REJECTED',
}
