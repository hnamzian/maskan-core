import { BaseRepository } from '../core/doc-mangement/base.repository';
import { ExecRequestEntity } from './exec-request.entity';

export class ExecRequestRepository extends BaseRepository<ExecRequestEntity> {
  protected getKey(requestId: string): string {
    return `EXCRQ_${requestId}`;
  }
}
