import { Context } from 'fabric-contract-api';
import { ExecRequestEntity, ExecRequestStatus, ExecRequestType } from './exec-request.entity';
import { ExecRequestRepository } from './exec-request.repository';

export class ExecRequestProvider {
  private ctx: Context;
  private execRequestRepository: ExecRequestRepository;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.execRequestRepository = new ExecRequestRepository(this.ctx, ExecRequestEntity);
  }

  public async getRequest(requestId: string): Promise<ExecRequestEntity> {
    return await this.execRequestRepository.get(requestId);
  }

  public async createRequest(requestId: string, walletId: string, personId: string, requestType: ExecRequestType, requestData: string): Promise<ExecRequestEntity> {
    const request = await this.execRequestRepository.create({
      id: requestId,
      walletId,
      personId,
      requestType,
      requestData,
      status: ExecRequestStatus.created,
    } as ExecRequestEntity);

    return  request;
  }

  public async finishRequest(requestId: string): Promise<ExecRequestEntity> {
    let request = await this.execRequestRepository.get(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }

    request = await this.execRequestRepository.update({
      id: requestId,
      status: ExecRequestStatus.done,
    } as ExecRequestEntity);

    await this.execRequestRepository.delete(requestId);

    return request;
  }
}
