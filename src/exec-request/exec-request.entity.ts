import { BaseEntity } from '../core/doc-mangement/base.entity';

export class ExecRequestEntity extends BaseEntity {
  public walletId: string;
  public personId: string;
  public requestData: string;
  public requestType: ExecRequestType;
  public status: ExecRequestStatus;
  public createdAt: string;
  public updatedAt: string;

  public toString() {
    return JSON.stringify(this);
  }

  public toBuffer() {
    return Buffer.from(this.toString());
  }

}

export interface IMultiSigRequest {
  requestId?: string;
  walletId?: string;
  requestData?: string;
  requestType?: ExecRequestType;
  status?: ExecRequestStatus;
  createdAt?: string;
  updatedAt?: string;
}

export enum ExecRequestStatus {
  created = 'CREATED',
  done = 'DONE',
}

export enum ExecRequestType {
  addSellOrder = 'ADD_SELL_ORDER',
}
