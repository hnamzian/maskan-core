import { BaseRepository } from '../core/doc-mangement/base.repository';
import { PayableSettlementEntity } from './payable-settlement.entity';

export class PayableSettlementRepository extends BaseRepository<PayableSettlementEntity> {
  public async getByQuery(filter: object) {
    const query = {
      selector: {
        ...filter,
        collection: this.getCollection(),
      },
    };
    return await super.getByQuery(query);
  }

  protected getKey(payableSettlementId: string): string {
    return `PAYABLE_SETTLEMENT_${payableSettlementId}`;
    }
}