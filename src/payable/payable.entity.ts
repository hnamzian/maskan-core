import { BaseEntity } from '../core/doc-mangement/base.entity';

export class PayableEntity extends BaseEntity {
  public purchaseId: string;
  public description: string;
  public price: number;
  public dueDate: number;
  public status: PayableStatus;

  public isCreated() {
    return this.status === PayableStatus.created;
  }
  public isCompleted() {
    return this.status === PayableStatus.completed;
  }
  public isFailed() {
    return this.status === PayableStatus.failed;
  }

}

export enum PayableStatus {
  created = 'CREATED',
  completed = 'COMPLETED',
  failed = 'FAILED',
}
