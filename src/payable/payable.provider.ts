import { Context } from 'fabric-contract-api';
import * as _ from 'lodash';
import { BaratBalanceEntity } from '../barat-token/barat-balance.entity';
import { BaratTokenTransactionEntity } from '../barat-token/barat-token-transaction.entity';
import { BaratTokenProvider } from '../barat-token/barat-token.provider';
import { ContextProvider } from '../core/context/context.provider';
import { MultiSigRequestEntity, RequestType, SignMethod } from '../multisig-request/multisig-request.entity';
import { MultiSigRequestProvider } from '../multisig-request/multisig-request.provider';
import { PurchaseProvider } from '../purchase/purchase.provider';
import { Task } from '../tasks/tasks.model';
import { TasksProvider } from '../tasks/tasks.provider';
import { ConsensusType } from '../utils/consensus';
import { WalletProvider } from '../wallet/wallet.provider';
import { PayableSettlementEntity, PayableSettlementStatus, PayableSettlementTransfer } from './payable-settlement.entity';
import { PayableSettlementRepository } from './payable-settlement.repository';
import { PayableEntity, PayableStatus } from './payable.entity';
import { PayableRepository } from './payable.repository';

export class PayableProvider {
  private ctx: Context;
  private payableRepository: PayableRepository;
  private payableSettlementRepository: PayableSettlementRepository;
  private walletService: WalletProvider;
  private multiSigRequestService: MultiSigRequestProvider;
  private tasksService: TasksProvider;

  constructor(ctx: Context) {
    this.ctx = ctx;
    this.payableRepository = new PayableRepository(this.ctx, PayableEntity);
    this.payableSettlementRepository = new PayableSettlementRepository(this.ctx, PayableSettlementEntity);
    this.walletService = new WalletProvider(this.ctx);
    this.multiSigRequestService = new MultiSigRequestProvider(this.ctx);
    this.tasksService = new TasksProvider(this.ctx);
  }

  public async getPayable(payableId: string): Promise<PayableEntity> {
    const payable = await this.payableRepository.get(payableId);
    return payable;
  }

  public async createPayable(payableId: string, purchaseId: string, price: number, dueDate: number, description: string) {
    const payableData = {
      id: payableId,
      description,
      price,
      dueDate,
      purchaseId,
      status: PayableStatus.created,
    } as PayableEntity;
    const payable = await this.payableRepository.create(payableData);
    return payable;
  }

  public async updatePayable(payable: PayableEntity) {
    return await this.payableRepository.update(payable);
  }

  public async getPayableSettlement(payableSettlementId: string): Promise<PayableSettlementEntity> {
    return await this.payableSettlementRepository.get(payableSettlementId);
  }

  public async createPayableSettlement(payableSettlementId: string, payableId: string, transfers: PayableSettlementTransfer[])
    : Promise<{ payableSettlement: PayableSettlementEntity, request: MultiSigRequestEntity, tasks: Task[] }> {
    // verify payable exists
    const payable = await this.getPayable(payableId);
    if (!payable) {
      throw new Error(`Payable with Id: ${payableId} not found`);
    }

    // verify payable status is pending
    if (payable.status !== PayableStatus.created) {
      throw new Error(`A settlement for payable with Id: ${payableId} cannot be created`);
    }

    // verify transfers
    if (transfers.length === 0) {
      throw new Error('Settlement transfers must be provided')
    }

    // convert transfer amount to number
    transfers = transfers.map((transfer) => {
      transfer.amount = parseFloat(transfer.amount.toString());
      return transfer;
    });

    // if (await dao.payableTransfer.exists(ctx, payableTransferId))
    //   throw new Error('Another payable transfer already exists for this payable')

    // parse rawFund
    const payableSettlements = await this.payableSettlementRepository.getByQuery({ payableId });
    if (payableSettlements.length > 0) {
      throw new Error(`A settlement for payable with Id: ${payableId} has already been requested`);
    }

    // get buery and seller from purchase
    const purchaseService = new PurchaseProvider(this.ctx);
    const { buyerId: from, sellerId: to } = await purchaseService.getPurchase(payable.purchaseId);

    // create payable transfer
    const payableSettlementData = {
      id: payableSettlementId,
      payableId,
      transfers,
      sender: from,
      receiver: to,
      status: PayableSettlementStatus.created,
    } as PayableSettlementEntity;
    const payableSettlement = await this.payableSettlementRepository.create(payableSettlementData);

    // create multi-sig request for approve settlement by sender
    const request = await this.createApproveSettlementBySenderRequest(payableSettlement);

    // create tasks
    const tasks = this.tasksService.updateTasks(request);

    // return result
    return { payableSettlement, request, tasks };
  }

  public async approvePayableSettlement(requestId: string,
                                        rawRequest: string,
                                        signedRequest: string,
                                        certificate: string,
                                        signingAlg: string,
                                        signMethod: SignMethod) {
    // verify signer to be registered and active
    await this.walletService.verifyWalletByCertificate(certificate);

    // verify corp wallet is still active
    let request = await this.multiSigRequestService.getRequest(requestId);
    if (!request) {
      throw new Error(`Request with Id: ${requestId} not found`);
    }
    await this.walletService.verifyWalletById(request.walletId);

    // Approve request
    request = await this.multiSigRequestService.signRequest(requestId, rawRequest, signedRequest, certificate, signingAlg, signMethod);

    // verify purchase exists
    const { id: payableSettlementId } = JSON.parse(rawRequest);
    const payableSettlement = await this.getPayableSettlement(payableSettlementId);
    if (!payableSettlement) {
      throw new Error(`Payable settlement with Id: ${payableSettlementId} not found`);
    }

    // get payable from database to update status if needed
    const payable = await this.getPayable(payableSettlement.payableId);

    // process settlement
    const {
      payable: updatedPayable,
      payableSettlement: updatedPayableSettlement,
      tokenTransfers } = await this.processSettlementRequest(payableSettlement, payable, request);

    // update requests
    let requests: MultiSigRequestEntity[] = [ request ];
    if (request.requestType === RequestType.approveSettlementBySender && request.isApproved()) {
      const receiverRequest = await this.createApprovePayableSettlementRequest(payableSettlement);
      requests = [ ...requests, receiverRequest];
    }

    // update tasks
    let tasks: Task[] = [];
    for (const requestItem of requests) {
      tasks = [ ...tasks, ...this.tasksService.updateTasks(requestItem) ];
    }

    return {
      payableSettlement: updatedPayableSettlement,
      payable: updatedPayable,
      tokenTransfers,
      requests,
      tasks,
    };

  }

  private async createApprovePayableSettlementRequest(settlement: PayableSettlementEntity) {
    if (settlement.isCreated()) {
      return await this.createApproveSettlementBySenderRequest(settlement);
    } else if (settlement.isApprovedBySender()) {
      return await this.createApproveSettlementByReceiverRequest(settlement);
    }
    return null;
  }
  private async createApproveSettlementBySenderRequest(settlement: PayableSettlementEntity) {
    const { id: senderId, signingRules } = await this.walletService.getWallet(settlement.sender);

    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${settlement.id}_${senderId}_${settlement.createdAt}`;
    const request = await this.multiSigRequestService.getRequest(requestId);
    if (request) {
      return request;
    }

    const requestData = _.pick(settlement, ['id', 'from', 'to', 'payableId', 'transfers']);
    const requestDataString = JSON.stringify(requestData);
    return await this.multiSigRequestService.createRequest(requestId,
                                                           senderId,
                                                           signingRules,
                                                           ConsensusType.signingRules,
                                                           RequestType.approveSettlementBySender,
                                                           requestDataString);
  }
  private async createApproveSettlementByReceiverRequest(settlement: PayableSettlementEntity) {
    const { id: receiverId, signingRules } = await this.walletService.getWallet(settlement.receiver);

    const now = ContextProvider.getTxTimestamp(this.ctx);
    const requestId = `${settlement.id}_${receiverId}_${settlement.createdAt}`;
    const request = await this.multiSigRequestService.getRequest(requestId);
    if (request) {
      return request;
    }

    const requestData = _.pick(settlement, ['id', 'sender', 'receiver', 'payableId', 'transfers']);
    const requestDataString = JSON.stringify(requestData);
    return await this.multiSigRequestService.createRequest(requestId,
                                                           receiverId,
                                                           signingRules,
                                                           ConsensusType.signingRules,
                                                           RequestType.approveSettlementByReceiver,
                                                           requestDataString);
  }

  private async processSettlementRequest(payableSettlement: PayableSettlementEntity, payable: PayableEntity, request: MultiSigRequestEntity) {
    if (request.requestType === RequestType.approveSettlementBySender) {
      if (request.isApproved()) {
        payableSettlement.status = PayableSettlementStatus.approvedBySender;
      } else if (request.isRejected()) {
        payableSettlement.status = PayableSettlementStatus.rejected;
      }
    } else if (request.requestType === RequestType.approveSettlementByReceiver) {
      if (request.isApproved()) {
        payableSettlement.status = PayableSettlementStatus.approved;
      } else if (request.isRejected()) {
        payableSettlement.status = PayableSettlementStatus.rejected;
      }
    } else {
      throw new Error(`Improper request for processing payable settlement`);
    }

    // transfer tokens
    const baratTokenService = new BaratTokenProvider(this.ctx);
    const tokenTransfers: BaratTokenTransactionEntity[] = [];
    if (payableSettlement.status === PayableSettlementStatus.approved) {
      for (const transfer of payableSettlement.transfers)  {
        try {
          const { transaction, fromBalance, toBalance } = await baratTokenService.transfer(transfer.transferId,
                                                transfer.tokenId,
                                                payableSettlement.sender,
                                                payableSettlement.receiver,
                                                transfer.amount);
          tokenTransfers.push(transaction);
        } catch (ex) {
          payableSettlement.status = PayableSettlementStatus.failed;
        }
      }
      if (payableSettlement.status === PayableSettlementStatus.failed) {
        await this.payableSettlementRepository.delete(payableSettlement.id);
      } else {
        payableSettlement.status = PayableSettlementStatus.finalized;
        await this.payableSettlementRepository.update(payableSettlement);
      }
    }

    // update payable status
    if (payableSettlement.status === PayableSettlementStatus.failed) {
      payable.status = PayableStatus.failed;
      await this.payableRepository.delete(payable.id);
    } else if (payableSettlement.status === PayableSettlementStatus.finalized) {
      payable.status = PayableStatus.completed;
      await this.payableRepository.update(payable);
    }

    return {
      payable,
      payableSettlement,
      tokenTransfers,
    };
  }

}
