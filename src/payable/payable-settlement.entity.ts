import { BaseEntity } from '../core/doc-mangement/base.entity';

export class PayableSettlementEntity extends BaseEntity {
  public payableId: string;
  public transfers: PayableSettlementTransfer[];
  public sender: string;
  public receiver: string;
  public rejectedBy: string;
  public status: PayableSettlementStatus;
  public createdBy: string;
  public createdAt: string;
  public updatedAt: string;

  public isCreated() {
    return this.status === PayableSettlementStatus.created;
  }
  public isApprovedBySender() {
    return this.status === PayableSettlementStatus.approvedBySender;
  }
  public isApproved() {
    return this.status === PayableSettlementStatus.approved;
  }
  public isRejected() {
    return this.status === PayableSettlementStatus.rejected;
  }
  public isFailed() {
    return this.status === PayableSettlementStatus.failed;
  }
}

export interface PayableSettlementTransfer {
  transferId: string;
  tokenId: string;
  amount: number;
}

export enum PayableSettlementStatus {
  created = 'CREATED',
  approvedBySender = 'APPROVED_BY_SENDER',
  approved = 'APPROVED',
  finalized = 'FINALIZED',
  rejected = 'REJECTED',
  failed = 'FAILED',
}
