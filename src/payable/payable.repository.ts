import { BaseRepository } from "../core/doc-mangement/base.repository";
import { PayableEntity } from "./payable.entity";

export class PayableRepository extends BaseRepository<PayableEntity> {
  protected getKey(payableId: string): string {
    return `PAYABLE_${payableId}`;
  }
}
